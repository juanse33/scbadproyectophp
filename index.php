<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
session_start();
//error_reporting(0);
//ini_set("display_errors", 1);
require("business/Administrator.php");
require("business/LogAdministrator.php");
require("business/Profesor.php");
require("business/Tipo.php");
require("business/Inscripcion.php");
require("business/Horario.php");
require("business/Asignatura.php");
require("business/Grupo.php");
require("business/Asistencia.php");
require("business/ExcepcionPersonal.php");
require("business/Excepcion.php");
require("business/LogCoordinador.php");
require("business/Coordinador.php");
require("business/Inasistencia.php");
ini_set("display_errors", "1");
$webPages = array(
	'ui/recoverPassword.php',
	'ui/sessionAdministrator.php',
	'ui/administrator/insertAdministrator.php',
	'ui/administrator/updateAdministrator.php',
	'ui/administrator/selectAllAdministrator.php',
	'ui/administrator/searchAdministrator.php',
	'ui/administrator/updateProfileAdministrator.php',
	'ui/administrator/updatePasswordAdministrator.php',
	'ui/administrator/updatePictureAdministrator.php',
	'ui/administrator/cargaDatos.php',
	'ui/administrator/cargaDatosAjax.php',
	'ui/logAdministrator/searchLogAdministrator.php',
	'ui/profesor/insertProfesor.php',
	'ui/profesor/updateProfesor.php',
	'ui/profesor/selectAllProfesor.php',
	'ui/profesor/searchProfesor.php',
	'ui/inscripcion/selectAllInscripcionByProfesor.php',
	'ui/excepcionPersonal/selectAllExcepcionPersonalByProfesor.php',
	'ui/asistencia/selectAllAsistenciaByProfesor.php',
	'ui/inasistencia/selectAllInasistenciaByProfesor.php',
	'ui/tipo/insertTipo.php',
	'ui/tipo/updateTipo.php',
	'ui/tipo/selectAllTipo.php',
	'ui/tipo/searchTipo.php',
	'ui/excepcion/selectAllExcepcionByTipo.php',
	'ui/excepcionPersonal/selectAllExcepcionPersonalByTipo.php',
	'ui/inscripcion/insertInscripcion.php',
	'ui/inscripcion/updateInscripcion.php',
	'ui/inscripcion/selectAllInscripcion.php',
	'ui/inscripcion/searchInscripcion.php',
	'ui/horario/selectAllHorarioByInscripcion.php',
	'ui/grupo/selectAllGrupoByInscripcion.php',
	'ui/horario/insertHorario.php',
	'ui/horario/updateHorario.php',
	'ui/horario/selectAllHorario.php',
	'ui/horario/searchHorario.php',
	'ui/asignatura/insertAsignatura.php',
	'ui/asignatura/updateAsignatura.php',
	'ui/asignatura/selectAllAsignatura.php',
	'ui/asignatura/searchAsignatura.php',
	'ui/grupo/selectAllGrupoByAsignatura.php',
	'ui/grupo/insertGrupo.php',
	'ui/grupo/updateGrupo.php',
	'ui/grupo/selectAllGrupo.php',
	'ui/grupo/searchGrupo.php',
	'ui/asistencia/insertAsistencia.php',
	'ui/asistencia/updateAsistencia.php',
	'ui/asistencia/selectAllAsistencia.php',
	'ui/asistencia/searchAsistencia.php',
	'ui/excepcionPersonal/insertExcepcionPersonal.php',
	'ui/excepcionPersonal/updateExcepcionPersonal.php',
	'ui/excepcionPersonal/selectAllExcepcionPersonal.php',
	'ui/excepcionPersonal/searchExcepcionPersonal.php',
	'ui/excepcion/insertExcepcion.php',
	'ui/excepcion/updateExcepcion.php',
	'ui/excepcion/selectAllExcepcion.php',
	'ui/excepcion/searchExcepcion.php',
	'ui/logCoordinador/searchLogCoordinador.php',
	'ui/sessionCoordinador.php',
	'ui/coordinador/insertCoordinador.php',
	'ui/coordinador/updateCoordinador.php',
	'ui/coordinador/selectAllCoordinador.php',
	'ui/coordinador/searchCoordinador.php',
	'ui/coordinador/updateProfileCoordinador.php',
	'ui/coordinador/updatePasswordCoordinador.php',
	'ui/inasistencia/insertInasistencia.php',
	'ui/inasistencia/updateInasistencia.php',
	'ui/inasistencia/selectAllInasistencia.php',
	'ui/inasistencia/searchInasistencia.php',
	'ui/pdfAsistencia.php'
);
if (isset($_GET['logOut'])) {
	$_SESSION['id'] = "";
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<title>SCBAD</title>
	<link rel="shortcut icon"  href="img/logo copy.png" />
	<script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
	<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="icon" type="image/png" href="img/logo.png" />
	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" />
	<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.css" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
	<script>
		$(document).ready(function() {
			$('.selectpicker').selectpicker();
		});
	</script>
	<script charset="utf-8">
		$(function() {
			$("[data-toggle='tooltip']").tooltip();
		});
	</script>
</head>

<body>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
	<?php
	if (empty($_GET['pid'])) {
		include('ui/home.php');
	} else {
		$pid = base64_decode($_GET['pid']);
		if ($webPages[0] == $pid) {
			include($pid);
		} else {
			if ($_SESSION['id'] == "") {
				header("Location: index.php");
				die();
			}
			if ($_SESSION['entity'] == "Administrator") {
				include('ui/menuAdministrator.php');
			}
			if ($_SESSION['entity'] == "Coordinador") {
				include('ui/menuCoordinador.php');
			}
			if (in_array($pid, $webPages)) {
				include($pid);
			} else {
				include('ui/error.php');
			}
		}
	}
	?>
	<footer><div class="text-center text-muted">ITI &copy; <?php echo date("Y") ?></div></footer>
</body>

</html>