<?php
require("fpdf/fpdf.php");
require("business/Administrator.php");
require("business/LogAdministrator.php");
require("business/Profesor.php");
require("business/Tipo.php");
require("business/Inscripcion.php");
require("business/Horario.php");
require("business/Asignatura.php");
require("business/Grupo.php");
require("business/Asistencia.php");
require("business/ExcepcionPersonal.php");
require("business/Excepcion.php");
require("business/LogCoordinador.php");
require("business/Coordinador.php");
require("business/Inasistencia.php");
$profesor = "";
$estado = "";
if (isset($_GET['profesor'])) {
    $profesor = $_GET['profesor'];
}
if (isset($_GET['estado'])) {
    $estado = $_GET['estado'];
}
$inasistencia = new Inasistencia("", "", $estado, $profesor);
$inasistencias = $inasistencia->search();
$tamaño = count($inasistencias);
$objProfesor = new Profesor($profesor);
$objProfesor->select();
$pdf = new FPDF();
$pdf->AddPage();
$pdf->Image('img/logo1.jpeg', 29, 8, 35, 38);

$pdf->SetFont('Times', 'BI', 12);
if ($estado == 2) {
    $pdf->SetXY(95, 10);
    $pdf->Cell(40, 10, 'Universidad Distrital Francisco Jose de Caldas', 0, 0, 'C');
    $pdf->SetXY(95, 20);
    $pdf->Cell(40, 10, 'Reporte de Inasistencia por docente', 0, 0, 'C');
    $pdf->SetXY(95, 30);
    $pdf->Cell(40, 10, utf8_decode('Docente: ' . $objProfesor->getNombre()), 0, 0, 'C');
} else if ($estado == 1) {
    $pdf->SetXY(95, 10);
    $pdf->Cell(45, 10, 'Universidad Distrital Francisco Jose de Caldas', 0, 0, 'C');
    $pdf->SetXY(95, 20);
    $pdf->Cell(45, 10, 'Reporte de Inasistencias justificadas por docente', 0, 0, 'C');
    $pdf->SetXY(95, 30);
    $pdf->Cell(45, 10, utf8_decode('Docente: ' . $objProfesor->getNombre()), 0, 0, 'C');
} else if ($estado == 0) {
    $pdf->SetXY(95, 10);
    $pdf->Cell(45, 10, 'Universidad Distrital Francisco Jose de Caldas', 0, 0, 'C');
    $pdf->SetXY(95, 20);
    $pdf->Cell(45, 10, 'Reporte de Inasistencias injustificadas por docente', 0, 0, 'C');
    $pdf->SetXY(95, 30);
    $pdf->Cell(45, 10, utf8_decode('Docente: ' . $objProfesor->getNombre()), 0, 0, 'C');
}


if ($tamaño > 0) {
    if ($estado == 2) {
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->SetXY(37, 55);
        $pdf->SetFillColor(206, 214, 224);
        $pdf->Cell(47, 5, 'Fecha', 1, 0, 'C', True);
        $pdf->Cell(47, 5, 'Dia', 1, 0, 'C', True);
        $pdf->Cell(47, 5, 'Justificada', 1, 0, 'C', True);
        $pdf->Ln();
        $counter = 60;
        $pdf->SetFont('Arial', '', 12);
        foreach ($inasistencias as $currentInasistencia) {
            $dias = array('Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado');
            //echo "  ".date('N', strtotime($currentAsistencia->getFecha()))." ".$currentAsistencia->getFecha();
            $fechas = $dias[date('N', strtotime($currentInasistencia->getFecha()))];
            $pdf->SetXY(37, $counter);
            $pdf->Cell(47, 5, $currentInasistencia->getFecha(), 1, 0, 'C');
            $pdf->Cell(47, 5, $fechas, 1, 0, 'C');
            if($currentInasistencia-> getEstado()==0){
                $pdf->Cell(47, 5, "NO", 1, 0, 'C');
            }else{
                $pdf->Cell(47, 5, "SI", 1, 0, 'C');
            }
            $pdf->Ln();
            $counter = $counter + 5;
        }
    } else {
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->SetXY(45, 55);
        $pdf->SetFillColor(206, 214, 224);
        $pdf->Cell(60, 5, 'Fecha', 1, 0, 'C', True);
        $pdf->Cell(60, 5, 'Dia', 1, 0, 'C', True);
        $pdf->Ln();
        $counter = 60;
        $pdf->SetFont('Arial', '', 12);
        foreach ($inasistencias as $currentInasistencia) {
            $dias = array('Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado');
            //echo "  ".date('N', strtotime($currentAsistencia->getFecha()))." ".$currentAsistencia->getFecha();
            $fechas = $dias[date('N', strtotime($currentInasistencia->getFecha()))];
            $pdf->SetXY(45, $counter);
            $pdf->Cell(60, 5, $currentInasistencia->getFecha(), 1, 0, 'C');
            $pdf->Cell(60, 5, $fechas, 1, 0, 'C');
            $pdf->Ln();
            $counter = $counter + 5;
        }
    }
} else {
    $pdf->SetXY(75, 55);
    $pdf->SetFont('Arial', 'B', 25);
    $pdf->Cell(60, 5, 'NO SE ENCONTRARON RESULTADOS', 0, 0, 'C');
}
$pdf->Output();
