<?php

$fecha = "";
$profesor = "";
if (isset($_GET['fecha'])) {
	$fecha = $_GET['fecha'];
}
if (isset($_GET['profesor'])) {
	$profesor = $_GET['profesor'];
}
$asistencia = new Asistencia("", $fecha, $profesor);
$asistencias = $asistencia->search();
$tamaño = count($asistencias);
if ($tamaño > 0) {
?>
<script>
	/*function habilitar(){ 
		var boton = document.getElementById("pdf");
	}*/
	$("pdf").prop('disabled', true);
</script>
	<script charset="utf-8">
		$(function() {
			$("[data-toggle='tooltip']").tooltip();
		});
	</script>

	<table class="table table-borderless table-hover " id="table">
		<thead>
			<tr>
				<th></th>
				<th >Dia</th>
				<th nowrap>Fecha</th>
				<th>Profesor</th>
			</tr>
		</thead>
		</tbody>
		<?php

		$counter = 1;
		foreach ($asistencias as $currentAsistencia) {
			echo "<tr><td>" . $counter . "</td>";
			$dias = array('Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado');
			$fechas = $dias[date('N', strtotime($currentAsistencia->getFecha()))];
			echo "<td>" . $fechas . "</td>";
			echo "<td>" . $currentAsistencia->getFecha() . "</td>";
			echo "<td>" . $currentAsistencia->getProfesor()->getNombre() . "</td>";

			echo "</tr>";
			$counter++;
		}
		?>
		
		</tbody>
		
	</table>
	
	<br />
	</div>
	</div>
	</div>
<?php
} else {
?>
	<center>
		<div class="container">
			<h4>No se encontraron resultados</h4>
		</div>
	</center>
	<br/>
<?php
}
?>