<?php
	$opcion = "";
	if (isset($_GET['opcion'])) {
		$opcion = $_GET['opcion'];
	}

?>

<div class="container">
	<div class="row">
		<?php
		if (isset($_GET['opcion'])) {
			if ($opcion == 1) {
		?>
				<div class="col-md-4 offset-4">
					<input type="date" class="form-control" id="fecha" autocomplete="off" />
				</div>
			<?php
			} else {
			?>
				<div class="col-md-6 offset-4">
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<select class="selectpicker" data-live-search="true" style="width:3444em;" id="profesor" name="profesor">
						<option selected disabled value="0">Seleccione el profesor</option>
						<?php
						$objProfesor = new Profesor();
						$profesors = $objProfesor->selectAll();
						foreach ($profesors as $currentProfesor) {
							echo "<option value='" . $currentProfesor->getIdProfesor() . "'>" . $currentProfesor->getNombre() .  "</option>";
						}
						?>
					</select>
				</div>
		<?php
			}
		}
		?>
	</div>
	<br/>
	<div id="searchResult"></div>
</div>

<script> 
$(document).ready(function() {
$('.selectpicker').selectpicker();
     });
</script>
<script>
	$(document).ready(function() {
		$("#profesor").change(function() {
			$('#boton').html('<a href="pdfAsistencia.php?profesor=+' + $("#profesor").val() + '" target="_onblack" data-toggle="tooltip" data-placement="right" class="tooltipLink" data-original-title="Generar pdf"><img src="img/pdf1.png" width="32px"></a>');
		});
		$("#fecha").change(function() {
			if ($("#fecha").val() != "") {
				var path = "indexAjax.php?pid=<?php echo base64_encode("ui/asistencia/searchAsistenciaAjax.php"); ?>&fecha=" + $("#fecha").val();
				$("#searchResult").load(path);
			}
		});
	});
</script>

<script>
	$(document).ready(function() {
		$("#profesor").change(function() {
			if ($("#profesor").val() != "") {
				var path = "indexAjax.php?pid=<?php echo base64_encode("ui/asistencia/searchAsistenciaAjax.php"); ?>&profesor=+" + $("#profesor").val();
				$("#searchResult").load(path);
			}
		});
	});
</script>