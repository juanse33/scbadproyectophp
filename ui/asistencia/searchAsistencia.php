<?php
$opcion = "";
if (isset($_GET['opcion'])) {
	$opcion = $_GET['opcion'];
}

?>
<?php
$opcion = "";
if (isset($_GET['opcion'])) {
	$opcion = $_GET['opcion'];
}

?>
<style>
	@import url('https://fonts.googleapis.com/css?family=PT+Sans&display=swap');
	
	@import url('https://fonts.googleapis.com/css?family=Spartan&display=swap" rel="stylesheet');
	#titulo{
		font-family: 'Spartan', sans-serif;
		font-size:180%;
	}

	tbody {
		display: block;
		height: 280px;
		overflow: auto;
	}

	thead,
	tbody tr {
		display: table;
		width: 100%;
		table-layout: fixed;
	}



</style>

<div class="container-fluid" id="card">
	<div class="row">
		<div class="col">
			<br>
			<h1 id="titulo">
				<center>Asistencia</center>
			</h1>
		</div>
	</div>
	<div class="row">
		<div class="col">
			<div class="container">
				<div class="row">
					<div class="col-md-4 offset-4">
						<div class="icono">
							<select class="form-control" id="opcion" name="tipo">
								<br>
								<option selected disabled value="0">Seleccione el filtro</option>
								<option value="1">Por fecha</option>
								<option value="2">Por profesor</option>
							</select>
						</div>
					</div>
					<div class="col-md-2" id="boton" name="boton"></div>
					
				</div>
				<br>
				<div class="row">
					<div class="col">
						<div id="filtros"></div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$("#opcion").change(function() {
			var path = "indexAjax.php?pid=<?php echo base64_encode("ui/asistencia/filtroAsistenciaAjax.php"); ?>&opcion=" + $("#opcion").val();;
			$("#filtros").load(path);
		});
	});
</script>

