<?php
	$opcion = "";
	if (isset($_GET['opcion'])) {
		$opcion = $_GET['opcion'];
	}

?>
<div class="container">
	<div class="row">
		<?php
		if (isset($_GET['opcion'])) {
			if ($_GET['opcion'] == 1) {
		?>
				<div class="col-md-4 offset-4">
					<input type="date" class="form-control" id="fecha" autocomplete="off" />
				</div>
			<?php
			} else {
			?>

				<div class="col-md-6 offset-4">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<select class="selectpicker" data-live-search="true" style="width:3444em;" id="tipo" name="tipo">
						<option selected disabled value="0">Seleccione el tipo</option>
						<?php
						$objTipo = new Tipo();
						$tipos = $objTipo->selectAll();
						foreach ($tipos as $currentTipo) {
							echo "<option value='" . $currentTipo->getIdTipo() . "'";

							echo ">" . $currentTipo->getNombre() . "</option>";
						}
						?>
					</select>
				</div>

		<?php
			}
		}
		?>
	</div>
</div>
<script> 
$(document).ready(function() {
$('.selectpicker').selectpicker();
     });
</script>
<script>
	$(document).ready(function() {
		$("#tipo").change(function() {
			if ($("#tipo").val() != "") {
				var path = "indexAjax.php?pid=<?php echo base64_encode("ui/excepcion/searchExcepcionAjax.php"); ?>&tipo=" + $("#tipo").val();
				$("#searchResult").load(path);
			}
		});
	});
</script>
<script>
	$(document).ready(function() {
		$("#fecha").change(function() {
			if ($("#fecha").val() != "") {
				var path = "indexAjax.php?pid=<?php echo base64_encode("ui/excepcion/searchExcepcionAjax.php"); ?>&fecha=" + $("#fecha").val();
				$("#searchResult").load(path);
			}
		});
	});
</script>