<?php
$tipo = "";
$fecha = "";
if (isset($_GET['tipo'])) {
	$tipo = $_GET['tipo'];
}
if (isset($_GET['fecha'])) {
	$fecha = $_GET['fecha'];
}
$excepcion = new Excepcion("", "", $fecha, $tipo);
$excepcions = $excepcion->search();
if (count($excepcions) > 0) {
?>
<link href="css/tables.css" rel="stylesheet">
	<style>
		@import url('https://fonts.googleapis.com/css?family=PT+Sans&display=swap');

		tbody {
			display: block;
			height: 250px;
			overflow: auto;
		}

		thead,
		tbody tr {
			display: table;
			width: 100%;
			table-layout: fixed;
		}
	</style>
	<script charset="utf-8">
		$(function() {
			$("[data-toggle='tooltip']").tooltip();
		});
	</script>
	<div class="table-responsive">
		<table class="table table-borderless table-hover " id="table">
			<thead>
				<tr>
					<th></th>
					<th nowrap>Descripcion</th>
					<th nowrap>Fecha</th>
					<th nowrap>Huella</th>
					<th></th>
				</tr>
			</thead>
			</tbody>
			<?php

			$counter = 1;
			foreach ($excepcions as $currentExcepcion) {
				echo "<tr><td>" . $counter . "</td>";
				echo "<td>" . $currentExcepcion->getDescripcion() . "</td>";
				echo "<td>" . $currentExcepcion->getFecha() . "</td>";
				echo "<td>" . $currentExcepcion->getTipo()->getNombre() . "</td>";
				echo "<td class='text-center' nowrap>";
				echo "<a href='index.php?pid=" . base64_encode("ui/excepcion/updateExcepcion.php") . "&idExcepcion=" . $currentExcepcion->getIdExcepcion() . "'><span class='fas fa-edit' data-toggle='tooltip' data-placement='left' class='tooltipLink' data-original-title='Edit Excepcion' ></span></a> ";
				echo "</td>";
				echo "</tr>";
				$counter++;
			} ?>
			</tbody>

		</table>
	</div>
<?php
} else {
?>
	<center>
		<br/>
		<div class="container">
			<h4>No se encontraron resultados</h4>
		</div>
	</center>
<?php
}
?>