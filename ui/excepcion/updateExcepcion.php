<?php
$processed=false;
$idExcepcion = $_GET['idExcepcion'];
$updateExcepcion = new Excepcion($idExcepcion);
$updateExcepcion -> select();
$descripcion="";
if(isset($_POST['descripcion'])){
	$descripcion=$_POST['descripcion'];
}
$fecha=date("d/m/Y");
if(isset($_POST['fecha'])){
	$fecha=$_POST['fecha'];
}
$tipo="";
if(isset($_POST['tipo'])){
	$tipo=$_POST['tipo'];
}
if(isset($_POST['update'])){
	$updateExcepcion = new Excepcion($idExcepcion, $descripcion, $fecha, $tipo);
	$updateExcepcion -> update();
	$updateExcepcion -> select();
	$objTipo = new Tipo($tipo);
	$objTipo -> select();
	$nameTipo = $objTipo -> getNombre() ;
	$user_ip = getenv('REMOTE_ADDR');
	$agent = $_SERVER["HTTP_USER_AGENT"];
	$browser = "-";
	if( preg_match('/MSIE (\d+\.\d+);/', $agent) ) {
		$browser = "Internet Explorer";
	} else if (preg_match('/Chrome[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Chrome";
	} else if (preg_match('/Edge\/\d+/', $agent) ) {
		$browser = "Edge";
	} else if ( preg_match('/Firefox[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Firefox";
	} else if ( preg_match('/OPR[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Opera";
	} else if (preg_match('/Safari[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Safari";
	}
	if($_SESSION['entity'] == 'Administrator'){
		$logAdministrator = new LogAdministrator("","Edit Excepcion", "Descripcion: " . $descripcion . ";; Fecha: " . $fecha . ";; Tipo: " . $nameTipo, date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
		$logAdministrator -> insert();
	}
	else if($_SESSION['entity'] == 'Coordinador'){
		$logCoordinador = new LogCoordinador("","Edit Excepcion", "Descripcion: " . $descripcion . ";; Fecha: " . $fecha . ";; Tipo: " . $nameTipo, date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
		$logCoordinador -> insert();
	}
	$processed=true;
}
?>
<link href="css/insert.css" rel="stylesheet">
<form id="form" method="post" action="index.php?pid=<?php echo base64_encode("ui/excepcion/updateExcepcion.php") . "&idExcepcion=" . $idExcepcion ?>" class="bootstrap-form needs-validation">
	<div class="container" id="card">
		<div class="row">
			<div class="col-8 offset-md-2">
				<br>
				<br>
				<h2>
					<center>Editar Excepcion</center>
				</h2>
				<br>
				<div class="container">
					<?php if ($processed) { ?>
						<script>
						Swal.fire(
							'Informacion Correcta',
							'Informacion actualizada correctamente',
							'success'
							)
					 </script>
					<?php } ?>
					<div class="row">
						<div class="col">
							<h5>Tipo</h5>
						</div>
						<div class="col">
							<h5>Fecha</h5>
						</div>
					</div>
					<div class="row">
						<div class="col">
							<select class="form-control" name="tipo">
								<br>
								<option selected disabled value="Seleccione el tipo">Seleccione el tipo</option>
								<?php
								$objTipo = new Tipo();
								$tipos = $objTipo->selectAll();
								foreach ($tipos as $currentTipo) {
									echo "<option value='" . $currentTipo->getIdTipo() . "'";
									if ($currentTipo->getIdTipo() == $updateExcepcion->getTipo()->getIdTipo()) {
										echo " selected";
									}
									echo ">" . $currentTipo->getNombre() . "</option>";
								}
								?>
							</select>

						</div>
						<div class="col">
						<input type="date" class="form-control" name="fecha" id="fecha" value="<?php echo $updateExcepcion->getFecha() ?>" autocomplete="off" />
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col">
							<h5>Descripcion</h5>
						</div>
					</div>
					<div class="row">
						<textarea class="form-control xd" id="descripcion" name="descripcion" ><?php echo $updateExcepcion->getDescripcion() ?></textarea>
						<!--script>
								$('#descripcion').summernote({

									tabsize: 2,
									height: 100,
								});
							</script>
					<input type="text" class="form-control" name="descripcion" id="descripcion" value="<?php echo $fecha ?>" autocomplete="off" /-->
					</div>
					<br>

				</div>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<br>
				<br>
				<center><button type="submit" class="btn btn-outline-secondary" style="width:22em;border-radius:15px;" name="update">Editar</button></center>
			</div>
		</div>
	</div>
</form>
