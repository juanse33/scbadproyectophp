<?php
$excepcion = new Excepcion();
$excepcions = $excepcion->selectAll();
?>

<style>
	@import url('https://fonts.googleapis.com/css?family=PT+Sans&display=swap');

	tbody {
		display: block;
		height: 290px;
		overflow: auto;
	}

	thead,
	tbody tr {
		display: table;
		width: 100%;
		table-layout: fixed;
	}
</style>

<div class="container-fluid" id="card">
	<div class="row">
		<div class="col">
			<br />
			<h1 id="titulo">
				<center>Excepeciones</center>
			</h1>
		</div>
	</div>
	<div class="row">
		<div class="col">
			<div class="container">
				<div class="row">
					<div class="col-md-4 offset-4">
						<div class="icono">
							<select class="form-control" id="opcion" name="tipo">
								<br>
								<option selected disabled value="0">Seleccione el filtro</option>
								<option value="1">Por fecha</option>
								<option value="2">Por tipo</option>
							</select>
						</div>
					</div>
				</div>
			</div>
			<br>
			<div id="filtros"></div>
		</div>
	</div>
	<div class="row">
		<br>
		<link href="css/tables.css" rel="stylesheet">
		<?php if (count($excepcions) > 0) { ?>
			<div class="container">
				<div class="table-responsive">
					<div id="searchResult">
						<table class="table table-borderless table-hover " id="table">
							<thead>
								<tr>
									<th></th>
									<th nowrap>Descripcion</th>
									<th nowrap>Fecha</th>
									<th nowrap>Huella</th>
									<th></th>
								</tr>
							</thead>
							</tbody>
							<?php

							$counter = 1;
							foreach ($excepcions as $currentExcepcion) {
								echo "<tr><td>" . $counter . "</td>";
								echo "<td>" . $currentExcepcion->getDescripcion() . "</td>";
								echo "<td>" . $currentExcepcion->getFecha() . "</td>";
								echo "<td>" . $currentExcepcion->getTipo()->getNombre() . "</td>";
								echo "<td class='text-center' nowrap>";
								if ($_SESSION['entity'] == 'Administrator') {
									echo "<a href='index.php?pid=" . base64_encode("ui/excepcion/updateExcepcion.php") . "&idExcepcion=" . $currentExcepcion->getIdExcepcion() . "'><span class='fas fa-edit' data-toggle='tooltip' data-placement='left' class='tooltipLink' data-original-title='Editar Excepcion' ></span></a> ";
								}
								echo "</td>";
								echo "</tr>";
								$counter++;
							}
							?>
							</tbody>

						</table>
					</div>
				</div>
			</div>
		<?php } else { ?>

			<br />
			<div class="container">
				<center>
					<h4>No se encontraron resultados</h4>
				</center>
			</div>


		<?php } ?>
	</div>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#opcion").change(function() {

				var path = "indexAjax.php?pid=<?php echo base64_encode("ui/excepcion/filtroExcepcionAjax.php"); ?>&opcion=" + $("#opcion").val();;
				$("#filtros").load(path);
			});
		});
	</script>