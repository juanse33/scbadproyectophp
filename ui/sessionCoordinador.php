<?php
$coordinador = new Coordinador($_SESSION['id']);
$coordinador -> select();
?>
<script>
	$(document).ready(function() {
		return alertify.success('Ingreso Exitoso');
	})
</script>
<style>
@import url('https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300&display=swap');
@import url('https://fonts.googleapis.com/css?family=Spartan&display=swap" rel="stylesheet');

#card{
	font-family: 'Spartan', sans-serif;
		color:rgb(47, 53, 66);
		font-size:90%;
}
h4, h6{
	color:darkgrey;
	font-size:100%;
}
p{
	font-size:120%;
	border-bottom:solid 1px grey;
	border-color:darkgrey;
}
p:hover{
	border-color:rgb(47, 53, 66);
}

</style>
<br/>
<div class="container " id="card">
	<div class="row">
		<div class="col">
		<br>
		<h1><center> Perfil</center></h1>
		</div>
		</div>
		<br>
	<div class="row">	
		<div class="col-3">
		<img src=<?php if($coordinador->getPicture() == "" ||$coordinador->getPicture()=="NULL" ) { echo "img/photo.svg"; } else { echo $coordinador->getPicture(); } ?> width="100%" style="border-radius: 300px">
		</div>
		<div class="col-9">
			<div class="container">
				<div class="row">
					<div class="col">
						<h4>Nombre</h4>
					</div>
					<div class="col">
						<h4>Apellido</h4>
					</div>
				</div>
				<div class="row">
					<div class="col">
						<p><?php echo $coordinador->getName() ?></p>
					</div>
					<div class="col">
					<p><?php echo $coordinador->getLastName() ?></p>
					</div>
				</div>
				
				<div class="row">
					<div class="col">
						<h4>Correo Electronico</h4>
					</div>
				</div>
				<div class="row">
					<div class="col">
					<p><?php echo $coordinador->getEmail() ?></p>
					</div>
				</div>
					
				
			</div>
		</div>
	</div>
</br>
</br>

	<div class="row">
		<div class="col">
		<h6><?php echo "Tu rol es: Coordinador"; ?></h6>
		</div>
	</div>
</div>
