<?php
$processed = false;
$nombre = "";
if (isset($_POST['nombre'])) {
	$nombre = $_POST['nombre'];
}
$apellido = "";
if (isset($_POST['apellido'])) {
	$apellido = $_POST['apellido'];
}
$correo = "";
if (isset($_POST['correo'])) {
	$correo = $_POST['correo'];
}
$huella = "";
if (isset($_POST['huella'])) {
	$huella = $_POST['huella'];
}
if (isset($_POST['insert'])) {
	$newProfesor = new Profesor("", $nombre . " " . $apellido, $correo, $huella);
	$newProfesor->insert();
	$user_ip = getenv('REMOTE_ADDR');
	$agent = $_SERVER["HTTP_USER_AGENT"];
	$browser = "-";
	if (preg_match('/MSIE (\d+\.\d+);/', $agent)) {
		$browser = "Internet Explorer";
	} else if (preg_match('/Chrome[\/\s](\d+\.\d+)/', $agent)) {
		$browser = "Chrome";
	} else if (preg_match('/Edge\/\d+/', $agent)) {
		$browser = "Edge";
	} else if (preg_match('/Firefox[\/\s](\d+\.\d+)/', $agent)) {
		$browser = "Firefox";
	} else if (preg_match('/OPR[\/\s](\d+\.\d+)/', $agent)) {
		$browser = "Opera";
	} else if (preg_match('/Safari[\/\s](\d+\.\d+)/', $agent)) {
		$browser = "Safari";
	}
	if ($_SESSION['entity'] == 'Administrator') {
		$logAdministrator = new LogAdministrator("", "Create Profesor", "Nombre: " . $nombre . ";; Apellido: " . $apellido . ";; Correo: " . $correo . ";; Huella: " . $huella, date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
		$logAdministrator->insert();
	} else if ($_SESSION['entity'] == 'Coordinador') {
		$logCoordinador = new LogCoordinador("", "Create Profesor", "Nombre: " . $nombre . ";; Apellido: " . $apellido . ";; Correo: " . $correo . ";; Huella: " . $huella, date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
		$logCoordinador->insert();
	}
	$processed = true;
}
?>
<link href="css/insert.css" rel="stylesheet">
<form id="form" method="post" action="index.php?pid=<?php echo base64_encode("ui/administrator/insertProfesor.php") ?>" class="bootstrap-form needs-validation">
	<div class="container " id="card">
		<div class="row">
			<div class="col-8 offset-md-2">
				<br>
				<br>
				<h2>
					<center>Crear Profesor</center>
				</h2>
				<br>
				<div class="container contenido">

					<div class="row">
						<div class="col">
							<h5>Nombre</h5>
						</div>
						<div class="col">
							<h5>Apellido</h5>
						</div>
					</div>
					<div class="row">
						<div class="col">
							<input type="text" class="form-control" name="nombre" value="<?php echo $nombre ?>" required />
						</div>
						<div class="col">
							<input type="text" class="form-control" name="apellido" value="<?php echo $apellido ?>" required />
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col">
							<h5>Correo</h5>
						</div>
					</div>
					<div class="row">
						<div class="col">
							<input type="email" class="form-control" name="correo" value="<?php echo $correo ?>" required />
						</div>
					</div>
					<br>

				</div>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<br>
				<br>
				<center><button type="submit" class="btn btn-outline-secondary" style="width:22em;border-radius:15px;font-size:1em;" name="insert">Crear</button></center>
			</div>
		</div>
	</div>
</form>