<script charset="utf-8">
	$(function() {
		$("[data-toggle='tooltip']").tooltip();
	});
</script>
<link href="css/tables.css" rel="stylesheet">
<div class="table-responsive">
	<div id="searchResult">
		<table id="dtVerticalScrollExample" class="table table-borderless table-hover " id="table">
			<thead>
				<tr>
					<th></th>
					<th nowrap>Nombre</th>
					<th nowrap>Huella</th>
					<th nowrap></th>
				</tr>
			</thead>

			<tbody>
				<?php
				$profesor = new Profesor();
				$profesors = $profesor->search($_GET['search']);
				$counter = 1;
				foreach ($profesors as $currentProfesor) {

					echo "<tr><td>" . $counter . "</td>";
					echo "<td>" . $currentProfesor->getNombre() . "</td>";
					echo "<td>";
					if (($currentProfesor->getHuella()) == NULL) {
						echo "<center><img src='img/cancelar.png' width='18px'></center>";
					} else {
						echo "<center><img src='img/comprobado.png' width='18px'></center>";
					}
					echo "<td class='text-right' nowrap>";
					echo "<a href='index.php?pid=" . base64_encode("ui/excepcionPersonal/selectAllExcepcionPersonalByProfesor.php") . "&idProfesor=" . $currentProfesor->getIdProfesor() . "'><span class='fas fa-search-plus' data-toggle='tooltip' data-placement='left' class='tooltipLink' data-original-title='Consultar Excepciones' ></span></a> ";
					if ($_GET['entity'] == 'Administrator') {
						echo "<a href='index.php?pid=" . base64_encode("ui/excepcionPersonal/insertExcepcionPersonal.php") . "&idProfesor=" . $currentProfesor->getIdProfesor() . "'><span class='fas fa-pen' data-toggle='tooltip' data-placement='left' class='tooltipLink' data-original-title='Crear Excepciones Personales' ></span></a> ";
					}
					echo "<a href='modalEstadisticas.php?idProfesor=" . $currentProfesor->getIdProfesor() . "'  data-toggle='modal' data-target='#modalEstadisticas' ><span class='fas fa-chart-pie' data-toggle='tooltip' data-placement='left' class='tooltipLink' data-original-title='Estadisticas' ></span></a> ";
					echo "</td>";
					echo "</tr>";
					$counter++;
				}
				?>
			</tbody>
	</div>
	</table>
</div>