<?php
$order = "";
if (isset($_GET['order'])) {
	$order = $_GET['order'];
}
$dir = "";
if (isset($_GET['dir'])) {
	$dir = $_GET['dir'];
}
?>
<style>
	@import url('https://fonts.googleapis.com/css?family=PT+Sans&display=swap');

	tbody {
		display: block;
		height: 350px;
		overflow: auto;
	}

	thead,
	tbody tr {
		display: table;
		width: 100%;
		table-layout: fixed;
	}

</style>
<link href="css/tables.css" rel="stylesheet">
<div class="container-fluid" id="card">
	<div class="row">
		<div class="col">
			<br>
			<h1 id="titulo">
				<center>Profesores<center>
			</h1>
			<div class="col-md-2"></div>
			<div class="col-md-8 offset-2">
				<div class="icono">
					<input type="text" class="form-control" id="search" placeholder="Buscar profesor" autocomplete="off" />
				</div>
				<br/>
			</div>
		</div>
		<div class="row">
			<div class="col">

				<div class="table-responsive">
					<div id="searchResult">
						<table class="table table-borderless table-hover " id="table">
							<thead>
								<tr>
									<th></th>
									<th nowrap>Nombre</th>
									<th nowrap>Huella
									</th>
									<th nowrap></th>
								</tr>
							</thead>

							<tbody>
								<?php
								$profesor = new Profesor();
								if ($order != "" && $dir != "") {
									$profesors = $profesor->selectAllOrder($order, $dir);
								} else {
									$profesors = $profesor->selectAll();
								}
								$counter = 1;
								foreach ($profesors as $currentProfesor) {

									echo "<tr><td>" . $counter . "</td>";
									echo "<td>" . $currentProfesor->getNombre() . "</td>";
									echo "<td>";
									if (($currentProfesor->getHuella()) == NULL) {
										echo "<center><img src='img/cancelar.png' width='18px'></center>";
									} else {
										echo "<center><img src='img/comprobado.png' width='18px'></center>";
									}
									echo "<td class='text-right' nowrap>";
									echo "<a href='index.php?pid=" . base64_encode("ui/excepcionPersonal/selectAllExcepcionPersonalByProfesor.php") . "&idProfesor=" . $currentProfesor->getIdProfesor() . "'><span class='fas fa-search-plus' data-toggle='tooltip' data-placement='left' class='tooltipLink' data-original-title='Consultar Excepciones' ></span></a> ";
									if ($_SESSION['entity'] == 'Administrator') {
										echo "<a href='index.php?pid=" . base64_encode("ui/excepcionPersonal/insertExcepcionPersonal.php") . "&idProfesor=" . $currentProfesor->getIdProfesor() . "'><span class='fas fa-pen' data-toggle='tooltip' data-placement='left' class='tooltipLink' data-original-title='Crear Excepciones Personales' ></span></a> ";
									}
									echo "<a href='modalEstadisticas.php?idProfesor=" . $currentProfesor->getIdProfesor() . "'  data-toggle='modal' data-target='#modalEstadisticas' ><span class='fas fa-chart-pie' data-toggle='tooltip' data-placement='left' class='tooltipLink' data-original-title='Estadisticas' ></span></a> ";
									echo "</td>";
									echo "</tr>";
									$counter++;
								}
								?>
							</tbody>
					</div>
					</table>
				</div>
				<br />
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modalEstadisticas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" id="modalContent">
		</div>
	</div>
</div>
<script>
	$('body').on('show.bs.modal', '.modal', function(e) {
		var link = $(e.relatedTarget);
		$(this).find(".modal-content").load(link.attr("href"));
	});
</script>
<script>
	$(document).ready(function() {
		$("#search").keyup(function() {
			if ($("#search")) {
				var search = $("#search").val().replace(" ", "%20");
				var path = "indexAjax.php?pid=<?php echo base64_encode("ui/profesor/searchProfesorAjax.php"); ?>&search=" + search + "&entity=<?php echo $_SESSION['entity'] ?>";
				$("#searchResult").load(path);
			}
		});
	});
</script>