<?php
$year = date('y');
$mes = date('m');
$dia = date("d", mktime(0, 0, 0, $mes + 1, 0, $year)); //extrae el ultimo dia del mes  
$fechaInicio = date("Y") . "-" . $mes . "-01";
$fechaFin = date('Y') . "-" . $mes . "-" . $dia;

for ($i = $fechaInicio; $i <= $fechaFin; $i = date("Y-m-d", strtotime($i . "+ 1 days"))) {
    $excepcion = new Excepcion();
    $excepcion->selectByFecha($i);

    if ($excepcion->getIdExcepcion() == null) {
        $profesor = new Profesor();
        $profesoresD = $profesor->selectProfesorByDay($i);
        $profesoresA = $profesor->selectProfesorByAsistencia($i);
        
        foreach ($profesoresD as $pr) {
            if (!in_array($pr->getIdProfesor(), $profesoresA)) {
                $inasistencia = new  Inasistencia("", "", 0, $pr->getIdProfesor());
                $inasistencia->insert();
            }
        }
    }
}
