<?php
$coordinador = new Coordinador($_SESSION['id']);
$coordinador -> select();
?>
<link href="css/coordinador.css" rel="stylesheet">
   <div id="wrapper">
      <div id="sidebar-wrapper">
         <ul class="sidebar-nav nav-pills nav-stacked" id="menu">
            </br>
            <li>
               <a href="index.php?pid=<?php echo base64_encode("ui/sessionCoordinador.php") ?>">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<img src="img/casa2.png" width="32px"></a>
            </li>
            </br>
            <li>
               <a id="head" href="#" style="color:aliceblue" data-toggle="dropdown">&nbsp&nbsp&nbsp<img src=<?php if($coordinador->getPicture() == "" ||$coordinador->getPicture()=="NULL" )  { echo "img/photo.svg"; } else { echo $coordinador->getPicture(); } ?> width="26px" style="border-radius:25px"> &nbsp &nbsp &nbsp<?php echo $coordinador->getName()  ?><span class="caret"></span></a>
               <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                  <li><a style="color:aliceblue" class="dropdown-item" href="index.php?pid=<?php echo base64_encode("ui/coordinador/updateProfileCoordinador.php") ?>">&nbsp&nbsp&nbsp&nbspEditar Perfil</a></li>
                  <li> <a style="color:aliceblue" class="dropdown-item" href="index.php?pid=<?php echo base64_encode("ui/coordinador/updatePasswordCoordinador.php") ?>">&nbsp&nbsp&nbsp&nbspCambiar Contraseña</a></li>
               </ul>
			</li>		
            </br>
            <li>
               <a id="head" href="#" style="color:aliceblue; font-weight: bold;">&nbsp&nbsp&nbsp<img src="img/buscar_usuario.png" width="32px">&nbsp&nbsp&nbsp&nbspConsultar</a>
               <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                  <li><a style="color:aliceblue; font-weight: bold;" class="dropdown-item" href="index.php?pid=<?php echo base64_encode("ui/coordinador/selectAllCoordinador.php") ?>">&nbsp&nbsp&nbsp&nbspCoordinador</a></li>
                  <li><a style="color:aliceblue; font-weight: bold;" class="dropdown-item" href="index.php?pid=<?php echo base64_encode("ui/asistencia/searchAsistencia.php") ?>">&nbsp&nbsp&nbsp&nbspAsistencia</a></li>
                  <li><a style="color:aliceblue; font-weight: bold;" class="dropdown-item" href="index.php?pid=<?php echo base64_encode("ui/inasistencia/searchInasistencia.php") ?>">&nbsp&nbsp&nbsp&nbspInasistencia</a></li>
                  <li><a style="color:aliceblue; font-weight: bold;" class="dropdown-item" href="index.php?pid=<?php echo base64_encode("ui/profesor/selectAllProfesor.php") ?>">&nbsp&nbsp&nbsp&nbspProfesor</a></li>
               </ul>
            </li>
            </br>
            <li>
               <a id="head" href="#" style="color:aliceblue; font-weight: bold;">&nbsp&nbsp&nbsp<img src="img/login.png" width="32px">&nbsp&nbsp&nbsp&nbspLog</a>
               <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                  <li><a style="color:aliceblue; font-weight: bold;" class="dropdown-item" href="index.php?pid=<?php echo base64_encode("ui/logCoordinador/searchLogCoordinador.php") ?>">&nbsp&nbsp&nbsp&nbspLog Coordinador</a></li>
               </ul>
            </li>
            <br/>
            <li>
               <a id="head" href="index.php?logOut=1">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<img src="img/salida.png" width="34px" data-toggle='tooltip' data-placement='right' title='Salir'></a>
            </li>
            
         </ul>
      </div>
      <!-- Sidebar -->

      <!-- /#sidebar-wrapper -->

      <!-- Page Content -->
      <div id="page-content-wrapper">

         <!-- /#wrapper -->

         <!-- Bootstrap core JavaScript -->
         <script src="vendor/jquery/jquery.min.js"></script>
         <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

         <!-- Menu Toggle Script -->
         <script>
            $("#menu-toggle").click(function(e) {
               e.preventDefault();
               $("#wrapper").toggleClass("toggled");
            });
            $("#menu-toggle-2").click(function(e) {
               e.preventDefault();
               $("#wrapper").toggleClass("toggled-2");
               $('#menu ul').hide();
            });

            function initMenu() {
               $('#menu ul').hide();
               $('#menu ul').children('.current').parent().show();
               //$('#menu ul:first').show();
               $('#menu li a').click(
                  function() {
                     var checkElement = $(this).next();
                     if ((checkElement.is('ul')) && (checkElement.is(':visible'))) {
                        return false;
                     }
                     if ((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
                        $('#menu ul:visible').slideUp('normal');
                        checkElement.slideDown('normal');
                        return false;
                     }
                  }
               );
            }
            $(document).ready(function() {
               initMenu();
            });
         </script>
	