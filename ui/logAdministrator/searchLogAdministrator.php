
<link href="css/tables.css" rel="stylesheet">
<div class="container">
	<div class="row">
		<div class="col">
			<h1 id="titulo">
				<center>Acciones Administrador</center>
			</h1>
		</div>
	</div>

	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<input type="text" class="form-control" id="search" placeholder="Buscar acciones de Administrator" autocomplete="off" />
		</div>
	</div>

	<div id="searchResult"></div>

</div>
</div>
<script>
	$(document).ready(function() {
		$("#search").keyup(function() {
			if ($("#search").val().length > 2) {
				var search = $("#search").val().replace(" ", "%20");
				var path = "indexAjax.php?pid=<?php echo base64_encode("ui/logAdministrator/searchLogAdministratorAjax.php"); ?>&search=" + search;
				$("#searchResult").load(path);
			}
		});
	});
</script>