<?php
$processed=false;
$nombre="";
if(isset($_POST['nombre'])){
	$nombre=$_POST['nombre'];
}
$apellido="";
if(isset($_POST['apellido'])){
	$apellido=$_POST['apellido'];
}
$correo="";
if(isset($_POST['correo'])){
	$correo=$_POST['correo'];
}
$clave="";
if(isset($_POST['clave'])){
	$clave=$_POST['clave'];
}
$imagen="";
if(isset($_POST['imagen$imagen'])){
	$imagen=$_POST['imagen$imagen'];
}
$estado="";
if(isset($_POST['estado'])){
	$estado=$_POST['estado'];
}
if(isset($_POST['insert'])){
	$newCoordinador = new Coordinador("", $nombre, $apellido, $correo, $clave, null, 1);
	$newCoordinador -> insert();
	$user_ip = getenv('REMOTE_ADDR');
	$agent = $_SERVER["HTTP_USER_AGENT"];
	$browser = "-";
	if( preg_match('/MSIE (\d+\.\d+);/', $agent) ) {
		$browser = "Internet Explorer";
	} else if (preg_match('/Chrome[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Chrome";
	} else if (preg_match('/Edge\/\d+/', $agent) ) {
		$browser = "Edge";
	} else if ( preg_match('/Firefox[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Firefox";
	} else if ( preg_match('/OPR[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Opera";
	} else if (preg_match('/Safari[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Safari";
	}
	if($_SESSION['entity'] == 'Administrator'){
		$logAdministrator = new LogAdministrator("","Create Coordinador", "nombre: " . $nombre . ";; apellido: " . $apellido . ";; correo: " . $correo . ";; clave: " . $clave . ";; imagen$imagen: " . $imagen . ";; estado: " . 1, date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
		$logAdministrator -> insert();
	}
	else if($_SESSION['entity'] == 'Coordinador'){
		$logCoordinador = new LogCoordinador("","Create Coordinador", "nombre: " . $nombre . ";; apellido: " . $apellido . ";; correo: " . $correo . ";; clave: " . $clave . ";; imagen$imagen: " . $imagen . ";; estado: " . 1, date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
		$logCoordinador -> insert();
	}
	$processed=true;
}
?>
<link href="css/insert.css" rel="stylesheet">
<form id="form" method="post" action="index.php?pid=<?php echo base64_encode("ui/coordinador/insertCoordinador.php") ?>" class="bootstrap-form needs-validation">
<div class="container" id="card">
	<div class="row">
		<div class="col-8 offset-md-2">
		<br>
		<br>	
		<h2><center>Crear Coordinador</center>
			</h2>
			<br>
			<div class="container">
			<?php if($processed){ ?>
				<script>Swal.fire(
						'Informacion Correcta',
						'Coordinador registrado',
						'success'
					)</script>
					<?php } ?>
				<div class="row">
					<div class="col">
						<h5>Nombre</h5>
					</div>
					<div class="col">
						<h5>Apellido</h5>
					</div>
				</div>
				<div class="row">
					<div class="col">
						<input type="text" class="form-control" id="nombre" name="nombre" value="" required />
					</div>
					<div class="col">
						<input type="text" class="form-control" id="apellido" name="apellido" value="" required />
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col">
						<h5>Correo</h5>
					</div>
					<div class="col">
						<h5>Clave</h5>
					</div>
				</div>
				<div class="row">
					<div class="col">
						<input type="correo" class="form-control" id="correo"  name="correo" value="" required />
					</div>
						<div id="result" name="result"></div>
					<div class="col">
						<input type="clave" class="form-control" id="clave" name="clave" value="" required />
					</div>
				</div>
				<br>
				
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col">
			<br>
			<br>
			<center><button type="submit" class="btn btn-outline-secondary" style="width:22em;border-radius:15px;" name="insert">Crear</button></center>
		</div>
	</div>
</div>
</form>
<script>
	$('document').ready(function(){
		$('#correo').blur(function(){
			var search = $("#correo").val();
			var path = "indexAjax.php?pid=<?php echo base64_encode("ui/coordinador/validarCorreo.php"); ?>&search=" + search;
			$("#result").load(path);
		})
	});
</script>