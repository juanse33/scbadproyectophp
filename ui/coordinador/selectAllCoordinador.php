<?php
$order = "";
if (isset($_GET['order'])) {
	$order = $_GET['order'];
}
$dir = "";
if (isset($_GET['dir'])) {
	$dir = $_GET['dir'];
}
?>

<style>
	@import url('https://fonts.googleapis.com/css?family=PT+Sans&display=swap');

	tbody {
		display: block;
		height: 350px;
		overflow: auto;
	}

	thead,
	tbody tr {
		display: table;
		width: 100%;
		table-layout: fixed;
	}



	.form-control {

		width: 100%;
		border: 2px solid #aaa;
		margin: 8px 0;
		outline: none;
		padding: 8px;
		box-sizing: border-box;
		border-radius: 15px;
		padding-left: 40px;
	}
</style>
<link href="css/tables.css" rel="stylesheet">
<div class="container-fluid" id="card">
	<div class="row">
		<div class="col">
			<br>
			<h1 id="titulo">
				<center>Coordinadores<center>
			</h1>
			<br />
		</div>
	</div>
	<div class="row">
		<div class="col">
			<div class="table-responsive">
				<table class="table table-borderless table-hover" id="table">
					<thead>
						<tr>
							<th></th>
							<th nowrap>Nombre

							</th>
							<th nowrap>Apellido

							</th>
							<th nowrap>Correo

							</th>
							<th nowrap></th>
						</tr>
					</thead>
					</tbody>
					<?php
					$coordinador = new Coordinador();
					if ($order != "" && $dir != "") {
						$coordinadors = $coordinador->selectAllOrder($order, $dir);
					} else {
						$coordinadors = $coordinador->selectAll();
					}
					$counter = 1;
					foreach ($coordinadors as $currentCoordinador) {
						echo "<tr><td>" . $counter . "</td>";
						echo "<td>" . $currentCoordinador->getName() . "</td>";
						echo "<td>" . $currentCoordinador->getLastname() . "</td>";
						echo "<td>" . $currentCoordinador->getEmail() . "</td>";
						echo "<td class='text-right' nowrap>";
						echo "<a href='modalCoordinador.php?idCoordinador=" . $currentCoordinador->getIdCoordinador() . "'  data-toggle='modal' data-target='#modalCoordinador' ><span class='fas fa-eye' data-toggle='tooltip' data-placement='left' class='tooltipLink' data-original-title='Ver mas informacion' ></span></a> ";
						if ($_SESSION['entity'] == 'Administrator') {
							echo "<a href='index.php?pid=" . base64_encode("ui/coordinador/updateProfileCoordinador.php") . "&idCoordinador=" . $currentCoordinador->getIdCoordinador() . "'><span class='fas fa-edit' data-toggle='tooltip' data-placement='left' class='tooltipLink' data-original-title='Editar Coordinador' ></span></a> ";
						}
						echo "</td>";
						echo "</tr>";
						$counter++;
					}
					?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modalCoordinador" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content" id="modalContent">
			</div>
		</div>
	</div>
	<script>
		$('body').on('show.bs.modal', '.modal', function(e) {
			var link = $(e.relatedTarget);
			$(this).find(".modal-content").load(link.attr("href"));
		});
	</script>