<?php
$processed = false;
$error = 0;
if(isset($_POST['update'])){
	if($_POST['newPassword'] == $_POST['newPasswordConfirm']){
		$updateCoordinador = new Coordinador($_SESSION['id']);
		$updateCoordinador -> select();
		if($updateCoordinador -> getPassword() == md5($_POST['currentPassword'])){
			$updateCoordinador -> updatePassword($_POST['newPassword']);
			$user_ip = getenv('REMOTE_ADDR');
			$agent = $_SERVER["HTTP_USER_AGENT"];
			$browser = "-";
			if( preg_match('/MSIE (\d+\.\d+);/', $agent) ) {
				$browser = "Internet Explorer";
			} else if (preg_match('/Chrome[\/\s](\d+\.\d+)/', $agent) ) {
				$browser = "Chrome";
			} else if (preg_match('/Edge\/\d+/', $agent) ) {
				$browser = "Edge";
			} else if ( preg_match('/Firefox[\/\s](\d+\.\d+)/', $agent) ) {
				$browser = "Firefox";
			} else if ( preg_match('/OPR[\/\s](\d+\.\d+)/', $agent) ) {
				$browser = "Opera";
			} else if (preg_match('/Safari[\/\s](\d+\.\d+)/', $agent) ) {
				$browser = "Safari";
			}
			$logCoordinador = new LogCoordinador("","Change Password Coordinador", "", date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
			$logCoordinador -> insert();
			$processed = true;
		} else {
			$error = 2;
		}
	} else {
		$error = 1;
	}
}
?>
<link href="css/insert.css" rel="stylesheet">
<form id="form" method="post" action="index.php?pid=<?php echo base64_encode("ui/coordinador/updatePasswordCoordinador.php") ?>" class="bootstrap-form needs-validation">
	<div class="container " id="card">
		<?php if ($processed) { ?>
			<script>
						Swal.fire(
							'Informacion Correcta',
							'Informacion actualizada correctamente',
							'success'
							)
					 </script>
		<?php } else if ($error == 1) { ?>
			<div class="alert alert-danger">Error. <em>Nueva </em> y <em>Repetir nueva</em> deben ser iguales
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		<?php } else if ($error == 2) { ?>
			<div class="alert alert-danger">Error. <em>Actual</em> no corresponde a su contraseña actual
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		<?php } ?>
		<div class="row">
			<div class="col-8 offset-md-2">
				<br>
				<br>
				<h2>
					<center>Cambiar contrase</center>
				</h2>
				<br>
				<div class="container contenido">

					<div class="row">
						<div class="col">
							<h5>Actual</h5>
						</div>
					</div>
					<div class="row">
						<div class="col">
						<input type="password" class="form-control" name="currentPassword" required />
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col">
							<h5>Nueva</h5>
						</div>
						
					</div>
					<div class="row">
						<div class="col">
						<input type="password" class="form-control" name="newPassword" required />
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col">
							<h5>Repetir nueva</h5>
						</div>
					</div>
					<div class="row">
						<div class="col">
						<input type="password" class="form-control" name="newPasswordConfirm" required />
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<br>
				<br>
				<center><button type="submit" class="btn btn-outline-secondary" style="width:22em;border-radius:15px;font-size:1em;" name="update">Cambiar</button></center>
			</div>
		</div>
	</div>
</form>


