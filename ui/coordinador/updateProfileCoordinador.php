<?php
$processed=false;
$idCoordinador="";
if(isset($_GET['idCoordinador'])){
	$idCoordinador=$_GET['idCoordinador'];

	
}else{
	$idCoordinador=$_SESSION['id'];
	
}
$updateCoordinador = new Coordinador($idCoordinador);
$updateCoordinador -> select();
$nombre="";
if(isset($_POST['nombre'])){
	$nombre=$_POST['nombre'];
}
$apellido="";
if(isset($_POST['apellido'])){
	$apellido=$_POST['apellido'];
}
$correo="";
if(isset($_POST['correo'])){
	$correo=$_POST['correo'];
}
$clave="";
if(isset($_POST['clave'])){
	$clave=$_POST['clave'];
}
$state="";
if(isset($_POST['state'])){
	$state=$_POST['state'];
}
if(isset($_POST['update'])){
	$updateCoordinador = new Coordinador($idCoordinador, $nombre, $apellido);
	$updateCoordinador -> update();
	$updateCoordinador -> select();
	$user_ip = getenv('REMOTE_ADDR');
	$agent = $_SERVER["HTTP_USER_AGENT"];
	$browser = "-";
	if( preg_match('/MSIE (\d+\.\d+);/', $agent) ) {
		$browser = "Internet Explorer";
	} else if (preg_match('/Chrome[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Chrome";
	} else if (preg_match('/Edge\/\d+/', $agent) ) {
		$browser = "Edge";
	} else if ( preg_match('/Firefox[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Firefox";
	} else if ( preg_match('/OPR[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Opera";
	} else if (preg_match('/Safari[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Safari";
	}
	$logCoordinador = new LogCoordinador("","Edit Profile Coordinador", "Nombre: " . $nombre . ";; Apellido: " . $apellido . ";; Correo: " . $correo . ";; Clave: " . $clave . ";; State: " . $state, date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
	$logCoordinador -> insert();
	$processed=true;
}
?>
<link href="css/insert.css" rel="stylesheet">
<form id="form" method="post" action="index.php?pid=<?php echo base64_encode("ui/coordinador/updateProfileCoordinador.php") ?>" class="bootstrap-form needs-validation">
	<div class="container " id="card">
	<?php if($processed){ ?>
		<script>
						Swal.fire(
							'Informacion Correcta',
							'Informacion actualizada correctamente',
							'success'
							)
					 </script>
					<?php } ?>
		<div class="row">
			<div class="col-8 offset-md-2">
				<br>
				<br>
				<h2>
					<center>Editar perfil</center>
				</h2>
				<br>
				<div class="container contenido">

					<div class="row">
						<div class="col">
							<h5>Nombre</h5>
						</div>
						<div class="col">
							<h5>Apellido</h5>
						</div>
					</div>
					<div class="row">
						<div class="col">
						<input type="text" class="form-control" name="nombre" value="<?php echo $updateCoordinador -> getName() ?>" required />
						</div>
						<div class="col">
						<input type="text" class="form-control" name="apellido" value="<?php echo $updateCoordinador -> getLastname() ?>" required />
						</div>
					</div>
					
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<br>
				<br>
				<center><button type="submit" class="btn btn-outline-secondary" style="width:22em;border-radius:15px;font-size:1em;" name="update">Editar</button></center>
			</div>
		</div>
	</div>
</form>

