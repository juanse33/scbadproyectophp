<?php
if (isset($_POST['recover'])) {
	$foundEmail = false;
	$generatedPassword = "";
	if (!$foundEmail) {
		$recoverAdministrator = new Administrator();
		if ($recoverAdministrator->existEmail($_POST['email'])) {;
			$generatedPassword = rand(100000, 999999);
			$recoverAdministrator->recoverPassword($_POST['email'], $generatedPassword);
			$foundEmail = true;
		}
	}
	
	if (!$foundEmail) {
		$recoverCoordinador = new Coordinador();
		if ($recoverCoordinador->existEmail($_POST['email'])) {;
			$generatedPassword = rand(100000, 999999);
			$recoverCoordinador->recoverPassword($_POST['email'], $generatedPassword);
			$foundEmail = true;
		}
	}
	if ($foundEmail) {
		$to = $_POST['email'];
		$subject = "Password recovery for SCBAD";
		$from = "FROM: SCBAD <contact@itiud.org>";
		$message = "Your new password is: " . $generatedPassword;
		mail($to, $subject, $message, $from);
	}
}
?>
<style>
	body {

		background-image: url("img/fondoalo.jpeg");
		background-repeat: no-repeat;
		background-size: cover;
	}

	#fpassword {
		border-radius: 15px;
	}

	#recovery {
		width: 31em;
		border-radius: 15px;
	}

	.form-control {

		width: 100%;
		border: 2px solid #aaa;
		margin: 8px 0;
		outline: none;
		padding: 8px;
		box-sizing: border-box;
		transition: 0.3s;
		border-radius: 15px;
		padding-left: 40px;
	}

	.form-control:focus {
		border-color: rgb(39, 174, 96);
		box-shadow: 0 0 8px 0 rgb(39, 174, 96);
	}

	.icono {
		position: relative;
	}

	.icono i {
		position: absolute;
		left: 0;
		top: 3px;
		padding: 8px 11px;
		color: #aaa;
		transition: 0.3s;
	}

	.icono .form-control:focus+i {
		color: rgb(39, 174, 96);
	}


    @import url('https://fonts.googleapis.com/css?family=Source+Sans+Pro&display=swap');
	#p{
		font-family: 'MS Gotic';
	}

</style>
</br>
</br>
</br>
</br>
</br>
<section>
	<div align="center">
	</div>
	<div class="container">
		<div class="row">
			<div class="col-6 offset-3 text-center">
				<div id="fpassword" class="shadow card">
					<div class="card-body">
						<div class="row">
							<div class="col-md-6 offset-3 text-center">
								<img src="img/lock.jpg" width="250px" />
							</div>
						</div>
						
						<?php if (isset($_POST['recover'])) { ?>
							<div class="alert alert-success">If the email: <em><?php echo $_POST['email'] ?></em> was found in the system, a new password was sent</div>
						<?php } else { ?>
							<form id="form" method="post" action="index.php?pid=<?php echo base64_encode("ui/recoverPassword.php") ?>" class="bootstrap-form needs-validation">
								<div class="form-group">
									<div class="icono">
										<input id="email" type="email" class="form-control" name="email" placeholder="Email" autocomplete="off" required />
										<i id="icono" class="fa fa-envelope fa-lg fa-fw" aria-hidden="true"></i>
									</div>
								</div>
								<button id="recovery" type="submit" class="btn btn-outline-secondary" name="recover">Recuperar contraseña</button>
								<div class="form-group">
								<a style="color:rgb(127, 140, 141)" href="index.php?pid=<?php echo base64_encode("ui/home.php") ?>">Volver</a>
							</div>
							</form>
						<?php } ?>
					</div>
				</div>

			</div>
		</div>
</section>
</div>