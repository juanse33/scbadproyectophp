<?php
$processed=false;
$idAdministrador = "";
if(isset($_GET['idAdministrador'])){
	$idAdministrador=$_GET['idAdministrador'];
}else{
	$idAdministrador=$_SESSION['id'];
}
$updateAdministrator = new Administrator($idAdministrador);
$updateAdministrator -> select();
$name="";
if(isset($_POST['name'])){
	$name=$_POST['name'];
}
$lastName="";
if(isset($_POST['lastName'])){
	$lastName=$_POST['lastName'];
}
$email="";
if(isset($_POST['email'])){
	$email=$_POST['email'];
}
$phone="";
if(isset($_POST['phone'])){
	$phone=$_POST['phone'];
}
$mobile="";
if(isset($_POST['mobile'])){
	$mobile=$_POST['mobile'];
}
$state="";
if(isset($_POST['state'])){
	$state=$_POST['state'];
}
if(isset($_POST['update'])){
	$updateAdministrator = new Administrator($_SESSION['id'], $name, $lastName, $email, "", "", $phone, $mobile, $state);
	$updateAdministrator -> update();
	$updateAdministrator -> select();
	$user_ip = getenv('REMOTE_ADDR');
	$agent = $_SERVER["HTTP_USER_AGENT"];
	$browser = "-";
	if( preg_match('/MSIE (\d+\.\d+);/', $agent) ) {
		$browser = "Internet Explorer";
	} else if (preg_match('/Chrome[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Chrome";
	} else if (preg_match('/Edge\/\d+/', $agent) ) {
		$browser = "Edge";
	} else if ( preg_match('/Firefox[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Firefox";
	} else if ( preg_match('/OPR[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Opera";
	} else if (preg_match('/Safari[\/\s](\d+\.\d+)/', $agent) ) {
		$browser = "Safari";
	}
	$logAdministrator = new LogAdministrator("","Edit Profile Administrator", "Name: " . $name . ";; Last Name: " . $lastName . ";; Email: " . $email . ";; Phone: " . $phone . ";; Mobile: " . $mobile . ";; State: " . $state, date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
	$logAdministrator -> insert();
	$processed=true;
}
?>
<link href="css/insert.css" rel="stylesheet">
<form id="form" method="post" action="index.php?pid=<?php echo base64_encode("ui/administrator/updateProfileAdministrator.php") ?>" class="bootstrap-form needs-validation">
	<div class="container " id="card">
	<?php if($processed){ ?>
		<script>
						Swal.fire(
							'Informacion Correcta',
							'Informacion actualizada correctamente',
							'success'
							)
					 </script>
					<?php } ?>
		<div class="row">
			<div class="col-8 offset-md-2">
				<br>
				<br>
				<h2>
					<center>Editar perfil</center>
				</h2>
				<br>
				<div class="container contenido">

					<div class="row">
						<div class="col">
							<h5>Nombre</h5>
						</div>
						<div class="col">
							<h5>Apellido</h5>
						</div>
					</div>
					<div class="row">
						<div class="col">
							<input type="text" class="form-control" name="name" value="<?php echo $updateAdministrator -> getName() ?>" required />
						</div>
						<div class="col">
							<input type="text" class="form-control" name="lastName" value="<?php echo $updateAdministrator -> getLastName() ?>" required />	
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col">
							<h5>Correo</h5>
						</div>
						<div class="col">
							<h5>Telefono</h5>
						</div>
					</div>
					<div class="row">
						<div class="col">
							<input type="email" class="form-control" name="email" value="<?php echo $updateAdministrator -> getEmail() ?>"  required />	
						</div>
						<div class="col">
							<input type="text" class="form-control" name="phone" value="<?php echo $updateAdministrator -> getPhone() ?>"/>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col">
							<h5>Celular</h5>
						</div>
					</div>
					<div class="row">
						<div class="col">
							<input type="text" class="form-control" name="mobile" value="<?php echo $updateAdministrator -> getMobile() ?>"/>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<br>
				<br>
				<center><button type="submit" class="btn btn-outline-secondary" style="width:22em;border-radius:15px;font-size:1em;" name="update">Editar</button></center>
			</div>
		</div>
	</div>
</form>

