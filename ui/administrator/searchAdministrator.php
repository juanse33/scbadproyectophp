<style>
	@import url('https://fonts.googleapis.com/css?family=PT+Sans&display=swap');

	#card {
		font-family: 'PT Sans', sans-serif;
		font-size: 120%;
	}

	.form-control {
		
		width: 100%;
		border: 2px solid #aaa;
		margin: 8px 0;
		outline: none;
		padding: 8px;
		box-sizing: border-box;
		transition: 0.3s;
		border-radius: 15px;
		padding-left: 40px;
	}

	.form-control:focus {
		border-color:rgba(223, 230, 233);
		box-shadow: 0 0 8px 0 rgba(223, 230, 233);
	}

	.icono {
		position: relative;
	}

	.icono i {
		position: absolute;
		left: 0;
		top: 1px;
		padding: 8px 11px;
		color: #aaa;
		transition: 0.3s;
	}

	.icono .form-control:focus+i {
		color: rgba(223, 230, 233);
	}

</style>

<div class="container-fluid" id="card">
	<div class="row">
		<div class="col">
			<br>
			<h1>
				<center>Search Administrator</center>
			</h1>
		</div>
	</div>
	<div class="row">
		<div class="col">
			<div class="container">
				<div class="row">
					<div class="col-md-2"></div>
					<div class="col-md-8">
						<div class="icono">

							<input type="text" class="form-control" id="search" placeholder="Search Administrator" autocomplete="off" />
							<i id="icono" class="fas fa-search" aria-hidden="true"></i>
						</div>
					</div>
				</div>
			</div>
			<br>
			<br>
			<div id="searchResult"></div>
		</div>
	</div>
</div>

</div>
</div>
<script>
	$(document).ready(function() {
		$("#search").keyup(function() {
			if ($("#search").val().length > 2) {
				var search = $("#search").val().replace(" ", "%20");
				var path = "indexAjax.php?pid=<?php echo base64_encode("ui/administrator/searchAdministratorAjax.php"); ?>&search=" + search + "&entity=<?php echo $_SESSION['entity'] ?>";
				$("#searchResult").load(path);
			}
		});
	});
</script>