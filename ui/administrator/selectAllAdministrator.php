<?php
$order = "lastName";
if (isset($_GET['order'])) {
	$order = $_GET['order'];
}
$dir = "asc";
if (isset($_GET['dir'])) {
	$dir = $_GET['dir'];
}
?>
<link href="css/tables.css" rel="stylesheet">

<div class="container-fluid" id="card">
	<div class="row">
		<div class="col">
			<br>
			<h1 id="titulo">
				<center>Administradores<center>
			</h1>
			<br/>
		</div>
	</div>
	<div class="row">
		<div class="col">
			<div class="table-responsive">
				<table class="table table-borderless table-hover" id="table">
					<thead>
						<tr>
							<th></th>
							<th nowrap>Nombre
							
							</th>
							<th nowrap>Apellido
								
							</th>
							<th nowrap>Correo
								
							</th>
							<th nowrap></th>
						</tr>
					</thead>
					</tbody>
					<?php
					$administrator = new Administrator();
					if ($order != "" && $dir != "") {
						$administrators = $administrator->selectAllOrder($order, $dir);
					} else {
						$administrators = $administrator->selectAll();
					}
					$counter = 1;
					foreach ($administrators as $currentAdministrator) {
						echo "<tr><td>" . $counter . "</td>";
						echo "<td>" . $currentAdministrator->getName() . "</td>";
						echo "<td>" . $currentAdministrator->getLastName() . "</td>";
						echo "<td>" . $currentAdministrator->getEmail() . "</td>";
						/*if($currentAdministrator->getState()==1){
							echo "<td>Activo</td>";
						}else{
							echo "<td>Desactivo</td>";	
						}*/

						echo "<td class='text-right' nowrap>";
						echo "<a href='modalAdministrator.php?idAdministrator=" . $currentAdministrator->getIdAdministrator() . "'  data-toggle='modal' data-target='#modalAdministrator' ><span class='fas fa-eye' data-toggle='tooltip' data-placement='left' class='tooltipLink' data-original-title='Ver mas informacion' ></span></a> ";
						if ($_SESSION['entity'] == 'Administrator') {
							echo "<a href='index.php?pid=" . base64_encode("ui/administrator/updateProfileAdministrator.php") . "&idAdministrator=" . $currentAdministrator->getIdAdministrator() . "'><span class='fas fa-edit' data-toggle='tooltip' data-placement='left' class='tooltipLink' data-original-title='Editar Administrador' ></span></a> ";
							echo "<a href='index.php?pid=" . base64_encode("ui/administrator/updatePictureAdministrator.php") . "&idAdministrator=" . $currentAdministrator->getIdAdministrator() . "&attribute=picture'><span class='fas fa-camera' data-toggle='tooltip' data-placement='left' class='tooltipLink' data-original-title='Editar imagen de perfil'></span></a> ";
						}
						echo "</td>";
						echo "</tr>";
						$counter++;
					}
					?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modalAdministrator" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content" id="modalContent">
			</div>
		</div>
	</div>
	<script>
		$('body').on('show.bs.modal', '.modal', function(e) {
			var link = $(e.relatedTarget);
			$(this).find(".modal-content").load(link.attr("href"));
		});
	</script>