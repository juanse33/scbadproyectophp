<?php 
$pro=false;
require 'vendor/autoload.php';	
use PhpOffice\PhpSpreadsheet\IOFactory;
if(isset($_POST['cargar'])){
    $localPath=$_FILES['dataFile']['tmp_name'];
    $type=$_FILES['dataFile']['type'];
    $serverPath = "files/datos.xlsx" ;
date_default_timezone_set('UTC');
if (file_exists('files/datos.xlsx')) {
    unlink('files/datos.xlsx');
}
copy($localPath,$serverPath);

$file_path = 'files/datos.xlsx';
$inputFileType = IOFactory::identify($file_path);
$objReader = IOFactory::createReader($inputFileType);
$objPHPExcel = $objReader->load($file_path);
$profesores = array();

$objWorksheet = $objPHPExcel->getActiveSheet();

for ($i = 2; $i < $objPHPExcel->setActiveSheetIndex(0)->getHighestRow(); $i++) {
    $nombre = trim($objPHPExcel->getActiveSheet()->getCell('A' . $i)->getValue());
    $codigo_asignatura = $objPHPExcel->getActiveSheet()->getCell('B' . $i)->getValue();
    $grupo = $objPHPExcel->getActiveSheet()->getCell('C' . $i)->getValue();
    $asignatura = $objPHPExcel->getActiveSheet()->getCell('D' . $i)->getValue();
    $asig = new Asignatura($codigo_asignatura,$asignatura);
    $asig -> insert();
    $profe = new Profesor("",$nombre);
    $profe->selectName();
    if($profe->getIdProfesor() == ""){
        $profe -> insert();
        $profe -> selectName();
    }

    $periodo = "".date('Y');
        if(date('m')<7)
            $periodo .= "-1";
        else
            $periodo .= "-2";
    $profe->selectName();
    $inscripcion = new Inscripcion("",$periodo,$profe->getIdProfesor());
    $inscripcion -> insert();
    $inscripcion -> selectByPeriodoProfesor();
    $gr= new Grupo($grupo,$asig->getIdAsignatura(),$inscripcion->getIdInscripcion());
    $gr -> insert();
    $dias=array();
    array_push($dias , $objPHPExcel->getActiveSheet()->getCell('H' . $i)->getValue().";");
    array_push($dias , $objPHPExcel->getActiveSheet()->getCell('I' . $i)->getValue().";");
    array_push($dias , $objPHPExcel->getActiveSheet()->getCell('J' . $i)->getValue().";");
    array_push($dias , $objPHPExcel->getActiveSheet()->getCell('K' . $i)->getValue().";");
    array_push($dias , $objPHPExcel->getActiveSheet()->getCell('L' . $i)->getValue().";");
    array_push($dias , $objPHPExcel->getActiveSheet()->getCell('M' . $i)->getValue().";");
    foreach($dias as $d){
        if($d !=";"){
        $dia = explode(" ",$d);
        $horario = new  Horario("", $dia[0],"",$inscripcion -> getIdInscripcion());  
        $horario -> insert();    
            }
        }
        $pro=true;
    }   
}
if($pro){
    echo "<script>Swal.fire(
        'Archivo cargado',
        'Base de datos actualizada',
        'success'
      )</script>";
}
?>

<style>
    @import url('https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300&display=swap');
    @import url('https://fonts.googleapis.com/css?family=Spartan&display=swap" rel="stylesheet');

    #card {
        color: rgb(47, 53, 66);
    }

    #titulo {
        font-family: 'Spartan', sans-serif;
        font-size: 180%;
    }
</style>

<br />
<link href="css/inserts.css" rel="stylesheet">
<form action="index.php?pid=<?php echo base64_encode("ui/administrator/cargaDatos.php") ?>" method="post" enctype="multipart/form-data">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1 id="titulo">
                    <center>Carga de datos</center>
                </h1>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-6 offset-3">
                <div class="alert alert-warning" role="alert">
                    <center>
                        <h4 class="alert-heading">Alerta!!</h4>
                    </center>
                    <hr>

                    <center>
                        <p> El archivo a cargar debe tener extencion .xlsx (Excel)</p>
                    </center>

                </div>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="custom-file">
                <div class="col-md-6 offset-3">
                    <input type="file" class="custom-file-input" name="image" id="image" >
							<label class="custom-file-label" for="inputGroupFile01">Selecciona</label>
                </div>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col">
                <center><button type="submit" value="cargar" name="cargar" class="btn btn-outline-dark" style="width:15em;border-radius:15px;">Cargar</button></center>
            </div>
            <br />
        </div>
    </div>
</form>
<script>
    $(document).on('change', 'input[type="file"]', function() {
        var fileName = this.files[0].name;
        var ext = fileName.split('.').pop();

        if (ext != 'xlsx') {
            Swal.fire(
                'Archivo Erroneo',
                'El archivo debe ser Excel (.xlsx)',
                'error'
            )
            this.value = ''; // reset del valor
            this.files[0].name = '';
        }
    });
</script>