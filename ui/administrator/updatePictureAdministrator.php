<?php
$processed = false;
$attribute = "picture";
if (isset($_GET['attribute'])) {
	$attribute = $_GET['attribute'];
}

if (isset($_GET['idAdministrator'])) {
	$idAdministrator = $_GET['idAdministrator'];
} else {
	$idAdministrator = $_SESSION['id'];
}
$error = 0;
if (isset($_POST['update'])) {
	$localPath = $_FILES['image']['tmp_name'];
	$type = $_FILES['image']['type'];
	$updateAdministrator = new Administrator($idAdministrator);
	$updateAdministrator->select();
	if (file_exists($updateAdministrator->getPicture())) {
		unlink($updateAdministrator->getPicture());
	}
	$serverPath = "image/" . time() . ".png";
	copy($localPath, $serverPath);
	$updateAdministrator->updateImage($attribute, $serverPath);
	$user_ip = getenv('REMOTE_ADDR');
	$agent = $_SERVER["HTTP_USER_AGENT"];
	$browser = "-";
	if (preg_match('/MSIE (\d+\.\d+);/', $agent)) {
		$browser = "Internet Explorer";
	} else if (preg_match('/Chrome[\/\s](\d+\.\d+)/', $agent)) {
		$browser = "Chrome";
	} else if (preg_match('/Edge\/\d+/', $agent)) {
		$browser = "Edge";
	} else if (preg_match('/Firefox[\/\s](\d+\.\d+)/', $agent)) {
		$browser = "Firefox";
	} else if (preg_match('/OPR[\/\s](\d+\.\d+)/', $agent)) {
		$browser = "Opera";
	} else if (preg_match('/Safari[\/\s](\d+\.\d+)/', $agent)) {
		$browser = "Safari";
	}
	if ($_SESSION['entity'] == 'Administrator') {
		$logAdministrator = new LogAdministrator("", "Edit picture in Administrator", "Picture: " . $attribute, date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
		$logAdministrator->insert();
	} else if ($_SESSION['entity'] == 'Coordinador') {
		$logCoordinador = new LogCoordinador("", "Edit picture in Administrator", "Picture: " . $attribute, date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
		$logCoordinador->insert();
	}
	$processed = true;
}

?>


<link href="css/insert.css" rel="stylesheet">
<form id="form" method="post" action="index.php?pid=<?php echo base64_encode("ui/administrator/updatePictureAdministrator.php") ?>&idAdministrator=<?php echo $idAdministrator ?>&attribute=<?php echo $attribute ?>" class="bootstrap-form needs-validation" enctype="multipart/form-data">
	<div class="container " id="card">
		<?php if ($processed) { ?>
			<script>
				Swal.fire(
					'Informacion Correcta',
					'Informacion actualizada correctamente',
					'success'
				)
			</script>
		<?php } ?>
		<div class="row">
			<div class="col-8 offset-md-2">
				<br>
				<br>
				<h2>
					<center>Cambiar imagen de perfil</center>
				</h2>
				<br>
				<div class="container contenido">
					<br>
					<div class="row">
						<div class="col">
							<h5>Imagen</h5>
						</div>

					</div>
					<div class="row">
						<div class="col">
							<input type="file" class="custom-file-input" name="image" id="image" >
							<label class="custom-file-label" for="inputGroupFile01">Selecciona</label>
							
						</div>
					</div>

				</div>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<br>
				<br>
				<center><button type="submit" class="btn btn-outline-secondary" style="width:22em;border-radius:15px;font-size:1em;" name="update">Cambiar</button></center>
			</div>
		</div>
	</div>
</form>