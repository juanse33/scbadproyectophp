<?php
$processed = false;
$name = "";
if (isset($_POST['name'])) {
	$name = $_POST['name'];
}
$lastName = "";
if (isset($_POST['lastName'])) {
	$lastName = $_POST['lastName'];
}
$email = "";
if (isset($_POST['email'])) {
	$email = $_POST['email'];
}
$password = "";
if (isset($_POST['password'])) {
	$password = $_POST['password'];
}
$phone = "";
if (isset($_POST['phone'])) {
	$phone = $_POST['phone'];
}
$mobile = "";
if (isset($_POST['mobile'])) {
	$mobile = $_POST['mobile'];
}
$state = "";
if (isset($_POST['state'])) {
	$state = $_POST['state'];
}
if (isset($_POST['insert'])) {
	$newAdministrator = new Administrator("", $name, $lastName, $email, $password, "", $phone, $mobile, 1);
	$newAdministrator->insert();
	$user_ip = getenv('REMOTE_ADDR');
	$agent = $_SERVER["HTTP_USER_AGENT"];
	$browser = "-";
	if (preg_match('/MSIE (\d+\.\d+);/', $agent)) {
		$browser = "Internet Explorer";
	} else if (preg_match('/Chrome[\/\s](\d+\.\d+)/', $agent)) {
		$browser = "Chrome";
	} else if (preg_match('/Edge\/\d+/', $agent)) {
		$browser = "Edge";
	} else if (preg_match('/Firefox[\/\s](\d+\.\d+)/', $agent)) {
		$browser = "Firefox";
	} else if (preg_match('/OPR[\/\s](\d+\.\d+)/', $agent)) {
		$browser = "Opera";
	} else if (preg_match('/Safari[\/\s](\d+\.\d+)/', $agent)) {
		$browser = "Safari";
	}
	if ($_SESSION['entity'] == 'Administrator') {
		$logAdministrator = new LogAdministrator("", "Create Administrator", "Name: " . $name . ";; Last Name: " . $lastName . ";; Email: " . $email . ";; Password: " . $password . ";; Phone: " . $phone . ";; Mobile: " . $mobile . ";; State: " . $state, date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
		$logAdministrator->insert();
	} else if ($_SESSION['entity'] == 'Coordinador') {
		$logCoordinador = new LogCoordinador("", "Create Administrator", "Name: " . $name . ";; Last Name: " . $lastName . ";; Email: " . $email . ";; Password: " . $password . ";; Phone: " . $phone . ";; Mobile: " . $mobile . ";; State: " . $state, date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
		$logCoordinador->insert();
	}
	$processed = true;
}
?>
<link href="css/insert.css" rel="stylesheet">
<form id="form" method="post" action="index.php?pid=<?php echo base64_encode("ui/administrator/insertAdministrator.php") ?>" class="bootstrap-form needs-validation">
	<div class="container " id="card">
	<?php if($processed){ ?>
					<script>
						Swal.fire(
							'Informacion Correcta',
							'Administrador registrado',
							'success'
							)
					 </script>
					<?php } ?>
		<div class="row">
			<div class="col-8 offset-md-2">
				<br>
				<br>
				<h2>
					<center>Crear Administrador</center>
				</h2>
				<br>
				<div class="container contenido">

					<div class="row">
						<div class="col">
							<h5>Nombre</h5>
						</div>
						<div class="col">
							<h5>Apellido</h5>
						</div>
					</div>
					<div class="row">
						<div class="col">
							<input type="text" class="form-control" name="name"  required />
						</div>
						<div class="col">
							<input type="text" class="form-control" name="lastName"  required />
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col">
							<h5>Correo</h5>
						</div>
						<div class="col">
							<h5>Clave</h5>
						</div>
					</div>
					<div class="row">
						<div class="col">
							<input type="email" class="form-control" name="email" id="email"  required />
						</div>
						<div id="result" name="result"></div>
						<div class="col">
							<input type="password" class="form-control" name="password" id="password" require />	
					</div>
					</div>
					<br>
					<div class="row">
						<div class="col">
							<h5>Telefono</h5>
						</div>
						<div class="col">
							<h5>Celular</h5>
						</div>
					</div>
					<div class="row">
						<div class="col">
							<input type="text" class="form-control" name="phone" value="<?php echo $phone ?>" />
						</div>
						<div class="col">
							<input type="text" class="form-control" name="mobile" value="<?php echo $mobile ?>" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<br>
				<br>
				<center><button type="submit" class="btn btn-outline-secondary" style="width:22em;border-radius:15px;font-size:1em;" name="insert">Crear</button></center>
			</div>
		</div>
	</div>
</form>
<script>
	$('document').ready(function(){
		$('#email').blur(function(){
			var search = $("#email").val();
			var path = "indexAjax.php?pid=<?php echo base64_encode("ui/administrator/validarCorreo.php"); ?>&search=" + search;
			$("#result").load(path);
		})
	});
</script>