<style>
	@import url('https://fonts.googleapis.com/css?family=PT+Sans&display=swap');

	.container-fluid {
		font-family: 'PT Sans';
	}

	tbody {
		display: block;
		height: 250px;
		overflow: auto;
	}

	thead,
	tbody tr {
		display: table;
		width: 100%;
		table-layout: fixed;
	}
</style>
<link href="css/tables.css" rel="stylesheet"> 
<div class="container-fluid">
	<div class="row">
		<div class="col">
			<br />
			<h1 id="titulo">
				<center>Excepeciones Personales</center>
			</h1>
		</div>
	</div>
	<div class="row">
		<div class="col">
			<div class="container">
				<div class="row">
					<div class="col-md-4 offset-4">
						<div class="icono">
							<select class="form-control" id="opcion" name="tipo">
								<br>
								<option selected disabled value="0">Seleccione el filtro</option>
								<option value="1">Por fecha</option>
								<option value="2">Por tipo</option>
								<option value="3">Por profesor</option>
							</select>
							<br>
						</div>
					</div>
				</div>
			</div>
		
			<div id="filtros"></div>
		</div>
	</div>
	<div class="row">
		<br>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$("#opcion").change(function() {
			
			var path = "indexAjax.php?pid=<?php echo base64_encode("ui/excepcionPersonal/filtroExcepcionPersonalAjax.php"); ?>&opcion=" + $("#opcion").val();;
			$("#filtros").load(path);
		});
	});
</script>
