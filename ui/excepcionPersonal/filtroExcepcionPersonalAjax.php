<?php
$opcion = "";
if (isset($_GET['opcion'])) {
	$opcion = $_GET['opcion'];
}

?>
<div class="container">
	<div class="row">
		<?php
		if (isset($_GET['opcion'])) {
			if ($_GET['opcion'] == 1) {
		?>
				<div class="col-md-4 offset-4">
					<input type="date" class="form-control" id="fecha" autocomplete="off" />
				</div>
			<?php
			}
			if ($_GET['opcion'] == 2) {
			?>

				<div class="col-md-6 offset-4">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<select class="selectpicker" data-live-search="true" style="width:3444em;" id="tipo" name="tipo">
						<option selected disabled value="0">Seleccione el tipo</option>
						<?php
						$objTipo = new Tipo();
						$tipos = $objTipo->selectAll();
						foreach ($tipos as $currentTipo) {
							echo "<option value='" . $currentTipo->getIdTipo() . "'";

							echo ">" . $currentTipo->getNombre() . "</option>";
						} ?>
					</select>
				</div>

			<?php
			}
			if ($_GET['opcion'] == 3) {
			?>
				<div class="col-md-6 offset-4">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<select class="selectpicker" data-live-search="true" style="width:3444em;" id="profesor" name="profesor">
						<option selected disabled value="0">Seleccione el profesor</option>
						<?php
						$objProfesor = new Profesor();
						$profesors = $objProfesor->selectAll();
						foreach ($profesors as $currentProfesor) {
							echo "<option value='" . $currentProfesor->getIdProfesor() . "'>" . $currentProfesor->getNombre() . "</option>";
						} ?>
					</select>
				</div>
		<?php
			}
		}
		?>
		
	</div>
	<br/>
	<div class="row">
		
			<div class="col">
				<div id="searchResult"></div>
			</div>
		</div>
</div>
<script>
	$(document).ready(function() {
		$('.selectpicker').selectpicker();
	});
</script>
<script>
	$(document).ready(function() {
		$("#tipo").change(function() {
			if ($("#tipo").val() != "") {
				var path = "indexAjax.php?pid=<?php echo base64_encode("ui/excepcionPersonal/searchExcepcionPersonalAjax.php"); ?>&tipo=" + $("#tipo").val();
				$("#searchResult").load(path);
			}
		});
	});
</script>
<script>
	$(document).ready(function() {
		$("#fecha").change(function() {
			if ($("#fecha").val() != "") {
				var path = "indexAjax.php?pid=<?php echo base64_encode("ui/excepcionPersonal/searchExcepcionPersonalAjax.php"); ?>&fecha=" + $("#fecha").val();
				$("#searchResult").load(path);
			}
		});
	});
</script>
<script>
	$(document).ready(function() {
		$("#profesor").change(function() {
			if ($("#profesor").val() != "") {
				var path = "indexAjax.php?pid=<?php echo base64_encode("ui/excepcionPersonal/searchExcepcionPersonalAjax.php"); ?>&profesor=" + $("#profesor").val();
				$("#searchResult").load(path);
			}
		});
	});
</script>