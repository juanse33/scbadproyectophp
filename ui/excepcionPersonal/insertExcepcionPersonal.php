<?php
$processed = false;
$descripcion = "";
if (isset($_POST['descripcion'])) {
	$descripcion = $_POST['descripcion'];
}
$fecha = date("d/m/Y");
if (isset($_POST['fecha'])) {
	$fecha = $_POST['fecha'];
}
$profesor = "";
if (isset($_POST['profesor'])) {
	$profesor = $_POST['profesor'];
}
if (isset($_GET['idProfesor'])) {
	$profesor = $_GET['idProfesor'];
}
$tipo = "";
if (isset($_POST['tipo'])) {
	$tipo = $_POST['tipo'];
}
if (isset($_GET['idTipo'])) {
	$tipo = $_GET['idTipo'];
}
if (isset($_POST['insert'])) {
	$newExcepcionPersonal = new ExcepcionPersonal("", $descripcion, $fecha, $profesor, $tipo);
	$newExcepcionPersonal->insert();
	$objProfesor = new Profesor($profesor);
	$objProfesor->select();
	$nameProfesor = $objProfesor->getNombre();
	$objTipo = new Tipo($tipo);
	$objTipo->select();
	$nameTipo = $objTipo->getNombre();
	$inasistencia = new Inasistencia("",$fecha,"",$profesor);
	$inasistencia -> updateExcepcion();
	$user_ip = getenv('REMOTE_ADDR');
	$agent = $_SERVER["HTTP_USER_AGENT"];
	$browser = "-";
	if (preg_match('/MSIE (\d+\.\d+);/', $agent)) {
		$browser = "Internet Explorer";
	} else if (preg_match('/Chrome[\/\s](\d+\.\d+)/', $agent)) {
		$browser = "Chrome";
	} else if (preg_match('/Edge\/\d+/', $agent)) {
		$browser = "Edge";
	} else if (preg_match('/Firefox[\/\s](\d+\.\d+)/', $agent)) {
		$browser = "Firefox";
	} else if (preg_match('/OPR[\/\s](\d+\.\d+)/', $agent)) {
		$browser = "Opera";
	} else if (preg_match('/Safari[\/\s](\d+\.\d+)/', $agent)) {
		$browser = "Safari";
	}
	if ($_SESSION['entity'] == 'Administrator') {
		$logAdministrator = new LogAdministrator("", "Create Excepcion Personal", "Descripcion: " . $descripcion . ";; Fecha: " . $fecha . ";; Profesor: " . $nameProfesor . ";; Tipo: " . $nameTipo, date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
		$logAdministrator->insert();
	} else if ($_SESSION['entity'] == 'Coordinador') {
		$logCoordinador = new LogCoordinador("", "Create Excepcion Personal", "Descripcion: " . $descripcion . ";; Fecha: " . $fecha . ";; Profesor: " . $nameProfesor . ";; Tipo: " . $nameTipo, date("Y-m-d"), date("H:i:s"), $user_ip, PHP_OS, $browser, $_SESSION['id']);
		$logCoordinador->insert();
	}
	$processed = true;
}
?>
<link href="css/insert.css" rel="stylesheet">
<?php if(!isset($_GET['idProfesor'])){ ?>
<form id="form" method="post" action="index.php?pid=<?php echo base64_encode("ui/excepcionPersonal/insertExcepcionPersonal.php") ?>" class="bootstrap-form needs-validation">
<?php }else{ ?>
	<form id="form" method="post" action="index.php?pid=<?php echo base64_encode("ui/excepcionPersonal/insertExcepcionPersonal.php")."&idProfesor=".$_GET['idProfesor']; ?>" class="bootstrap-form needs-validation">		
<?php } ?>
	<div class="container" id="card">
		<div class="row">
			<div class="col-8 offset-md-2">
				<br>
				<br>

				<h2>
					<?php $nProfesor = '';
					if(isset($_GET['idProfesor'])){
						$pr = new Profesor($_GET['idProfesor']);
						$pr -> select();
						$nProfesor = $pr -> getNombre();
						}?>
					<center>Crear Excepcion <?php echo $nProfesor; ?></center>
				</h2>
				<br>
				<div class="container">
					<?php if ($processed) { ?>
						<script>
						Swal.fire(
							'Informacion Correcta',
							'Exepcion personal registrado',
							'success'
							)
					 </script>
					<?php } ?>
					<div class="row">
						<div class="col">
							<h5>Fecha</h5>
						</div>
						<?php if(!isset($_GET['idProfesor'])){ ?>
							<div class="col">
							<h5>Profesor</h5>
						</div>
						<?php } ?>
					</div>
					<div class="row">
						<div class="col">
							<input type="date" class="form-control" name="fecha" id="fecha" value="<?php echo $fecha ?>" autocomplete="off" />
						</div>
						<?php if(!isset($_GET['idProfesor'])){ ?>
						<div class="col">
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<select class="selectpicker" data-live-search="true" style="width:3444em;" id="profesor" name="profesor">
								<option selected disabled value="0">Seleccione el profesor</option>
								<?php
								$objProfesor = new Profesor();
								$profesors = $objProfesor->selectAll();
								foreach ($profesors as $currentProfesor) {
									echo "<option value='" . $currentProfesor->getIdProfesor() . "'>" . $currentProfesor->getNombre() . "</option>";
								}
								?>
							</select>
						</div>
							<?php } ?>
					</div>
					<br>
					<div class="row">
						<div class="col">
							<h5>Tipo</h5>
						</div>
					</div>
					<div class="row">
						<div class="col">
							<select class="form-control" name="tipo">
								<option selected disabled value="">Seleccione el tipo</option>
								<?php
								$objTipo = new Tipo();
								$tipos = $objTipo->selectAll();
								foreach ($tipos as $currentTipo) {
									echo "<option value='" . $currentTipo->getIdTipo() . "'";
									if ($currentTipo->getIdTipo() == $tipo) {
										echo " selected";
									}
									echo ">" . $currentTipo->getNombre() . "</option>";
								}
								?>
							</select>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col">
							<h5>Descripcion</h5>
						</div>
					</div>
					<div class="row">
						<div class="col">
							<textarea class="form-control" id="descripcion" name="descripcion"></textarea>

						</div>
					</div>
					<br>

				</div>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<center><button type="submit" class="btn btn-outline-secondary" style="width:22em;border-radius:15px;" name="insert">Crear</button></center>
			</div>
		</div>
	</div>
</form>