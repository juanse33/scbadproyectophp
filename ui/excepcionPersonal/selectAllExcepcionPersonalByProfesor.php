<?php
$order = "";
if(isset($_GET['order'])){
	$order = $_GET['order'];
}
$dir = "";
if(isset($_GET['dir'])){
	$dir = $_GET['dir'];
}
$profesor = new Profesor($_GET['idProfesor']); 
$profesor -> select();
?>
<link href="css/tables.css" rel="stylesheet">
<div class="container-fluid" id="card">
	<div class="row">
		<div class="col">
			<br>
			<h1 id="titulo">
				<center>Excepciones Personales del Profesor: <?php echo $profesor -> getNombre() ?><center>
			</h1>
				<div class="row">
			<div class="col">

				<div class="table-responsive">
					<div id="searchResult">
						<table class="table table-borderless table-hover " id="table">
							<thead>

					<tr><th></th>
						<th nowrap>Descripcion</th>
						<th nowrap>Fecha</th>
						<th>Profesor</th>
						<th>Tipo</th>
						<th nowrap></th>
					</tr>
				</thead>
				</tbody>
					<?php
					$excepcionPersonal = new ExcepcionPersonal("", "", "", $_GET['idProfesor'], "");
					if($order!="" && $dir!="") {
						$excepcionPersonals = $excepcionPersonal -> selectAllByProfesorOrder($order, $dir);
					} else {
						$excepcionPersonals = $excepcionPersonal -> selectAllByProfesor();
					}
					$counter = 1;
					foreach ($excepcionPersonals as $currentExcepcionPersonal) {
						echo "<tr><td>" . $counter . "</td>";
						echo "<td>" . $currentExcepcionPersonal -> getDescripcion() . "</td>";
						echo "<td>" . $currentExcepcionPersonal -> getFecha() . "</td>";
						echo "<td>" . $currentExcepcionPersonal -> getProfesor() -> getNombre() . "</td>";
						echo "<td>" . $currentExcepcionPersonal -> getTipo() -> getNombre() . "</td>";
						echo "<td class='text-right' nowrap>";
						if($_SESSION['entity'] == 'Administrator') {
							echo "<a href='index.php?pid=" . base64_encode("ui/excepcionPersonal/updateExcepcionPersonal.php") . "&idExcepcionPersonal=" . $currentExcepcionPersonal -> getIdExcepcionPersonal() . "'><span class='fas fa-edit' data-toggle='tooltip' data-placement='left' class='tooltipLink' data-original-title='Edit Excepcion Personal' ></span></a> ";
						}
						echo "</td>";
						echo "</tr>";
						$counter++;
					};
					?>
				</tbody>
			</table>
			</div>
		</div>
	</div>
</div>
