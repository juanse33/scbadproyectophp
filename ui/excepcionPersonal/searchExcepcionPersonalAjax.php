<?php
$tipo = "";
$fecha = "";
$profesor = "";
if (isset($_GET['tipo'])) {
	$tipo = $_GET['tipo'];
}
if (isset($_GET['fecha'])) {
	$fecha = $_GET['fecha'];
}
if (isset($_GET['profesor'])) {
	$profesor = $_GET['profesor'];
}
$excepcionPersonal = new ExcepcionPersonal("", "", $fecha, $profesor, $tipo);
$excepcionPersonals = $excepcionPersonal->search();
if (count($excepcionPersonals) > 0) {
?>

	<script charset="utf-8">
		$(function() {
			$("[data-toggle='tooltip']").tooltip();
		});
	</script>
	<div class="table-responsive">

		<table class="table table-borderless table-hover " id="table">
			<thead>
				<tr>
					<th></th>
					<th nowrap>Descripcion</th>
					<th nowrap>Fecha</th>
					<th nowrap>Profesor</th>
					<th nowrap>Tipo</th>
					<th></th>
				</tr>
			</thead>
			</tbody>
			<?php
			$excepcionPersonal = new ExcepcionPersonal("", "", $fecha, $profesor, $tipo);
			$excepcionPersonals = $excepcionPersonal->search();
			$counter = 1;
			foreach ($excepcionPersonals as $currentExcepcionPersonal) {
				echo "<tr><td>" . $counter . "</td>";
				echo "<td>" . $currentExcepcionPersonal->getDescripcion() . "</td>";
				echo "<td>" . $currentExcepcionPersonal->getFecha() . "</td>";
				echo "<td>" . $currentExcepcionPersonal->getProfesor()->getNombre() . "</td>";
				echo "<td>" . $currentExcepcionPersonal->getTipo()->getNombre() . "</td>";
				echo "<td class='text-right' nowrap>";
				echo "<a href='index.php?pid=" . base64_encode("ui/excepcionPersonal/updateExcepcionPersonal.php") . "&idExcepcionPersonal=" . $currentExcepcionPersonal->getIdExcepcionPersonal() . "'><span class='fas fa-edit' data-toggle='tooltip' data-placement='left' class='tooltipLink' data-original-title='Editar Excepcion Personal' ></span></a> ";
				echo "</td>";
				echo "</tr>";
				$counter++;
			} ?>
			</tbody>

		</table>
	</div>
<?php
} else {
?>
	<center>
		<div class="container">
			<h4>No se encontraron resultados</h4>
		</div>
	</center>
<?php
}
?>