<?php
$administrator = new Administrator($_SESSION['id']);
$administrator->select();
?>
<link href="css/simple-sidebar.css" rel="stylesheet">
   <div id="wrapper">
      <div id="sidebar-wrapper">
         <ul class="sidebar-nav nav-pills nav-stacked" id="menu">

            </br>
            <li>
               <a href="index.php?pid=<?php echo base64_encode("ui/sessionAdministrator.php") ?>">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<img src="img/casa2.png" width="32px"></a>
            </li>
            </br>
            <li>
               <a id="head" href="#" style="color:aliceblue" data-toggle="dropdown">&nbsp&nbsp&nbsp<img src=<?php if($administrator->getPicture() == "") { echo "img/photo.svg"; } else { echo $administrator->getPicture(); } ?> width="26px" style="border-radius:25px"> &nbsp &nbsp &nbsp<?php echo $administrator->getName()  ?><span class="caret"></span></a>
               <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                  <li><a style="color:aliceblue" class="dropdown-item" href="index.php?pid=<?php echo base64_encode("ui/administrator/updateProfileAdministrator.php") ?>">&nbsp&nbsp&nbsp&nbspEditar Perfil</a></li>
                  <li> <a style="color:aliceblue" class="dropdown-item" href="index.php?pid=<?php echo base64_encode("ui/administrator/updatePasswordAdministrator.php") ?>">&nbsp&nbsp&nbsp&nbspCambiar Contraseña</a></li>
                  <li> <a style="color:aliceblue" class="dropdown-item" href="index.php?pid=<?php echo base64_encode("ui/administrator/updatePictureAdministrator.php") ?>">&nbsp&nbsp&nbsp&nbspCambiar imagen</a></li>
               </ul>
            </li>
            </br>
            <li>
               <a id="head" href="#" style="color:aliceblue; font-weight: bold;">&nbsp&nbsp&nbsp<img src="img/usuario.png" width="32px">&nbsp&nbsp&nbsp&nbspAgregar</a>
               <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                  <li><a style="color:aliceblue; font-weight: bold;" class="dropdown-item" href="index.php?pid=<?php echo base64_encode("ui/administrator/insertAdministrator.php") ?>">&nbsp&nbsp&nbsp&nbspAdministrator</a></li>
                  <li><a style="color:aliceblue; font-weight: bold;" class="dropdown-item" href="index.php?pid=<?php echo base64_encode("ui/coordinador/insertCoordinador.php") ?>">&nbsp&nbsp&nbsp&nbspCoordinador</a></li>
                  <li><a style="color:aliceblue; font-weight: bold;" class="dropdown-item" href="index.php?pid=<?php echo base64_encode("ui/excepcionPersonal/insertExcepcionPersonal.php") ?>">&nbsp&nbsp&nbsp&nbspExcepcion Personal</a></li>
                  <li><a style="color:aliceblue; font-weight: bold;" class="dropdown-item" href="index.php?pid=<?php echo base64_encode("ui/excepcion/insertExcepcion.php") ?>">&nbsp&nbsp&nbsp&nbspExcepcion</a></li>
               </ul>
            </li>

            </br>
            <li>
               <a id="head" href="#" style="color:aliceblue; font-weight: bold;">&nbsp&nbsp&nbsp<img src="img/buscar_usuario.png" width="32px">&nbsp&nbsp&nbsp&nbspConsultar</a>
               <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                  <li><a style="color:aliceblue; font-weight: bold;" class="dropdown-item" href="index.php?pid=<?php echo base64_encode("ui/administrator/selectAllAdministrator.php") ?>">&nbsp&nbsp&nbsp&nbspAdministrator</a></li>
                  <li><a style="color:aliceblue; font-weight: bold;" class="dropdown-item" href="index.php?pid=<?php echo base64_encode("ui/coordinador/selectAllCoordinador.php") ?>">&nbsp&nbsp&nbsp&nbspCoordinador</a></li>
                  <li><a style="color:aliceblue; font-weight: bold;" class="dropdown-item" href="index.php?pid=<?php echo base64_encode("ui/profesor/selectAllProfesor.php") ?>">&nbsp&nbsp&nbsp&nbspProfesor</a></li>
                  <li><a style="color:aliceblue; font-weight: bold;" class="dropdown-item" href="index.php?pid=<?php echo base64_encode("ui/asistencia/searchAsistencia.php") ?>">&nbsp&nbsp&nbsp&nbspAsistencia</a></li>
                  <li><a style="color:aliceblue; font-weight: bold;" class="dropdown-item" href="index.php?pid=<?php echo base64_encode("ui/inasistencia/searchInasistencia.php") ?>">&nbsp&nbsp&nbsp&nbspInasistencia</a></li>
                  <li><a style="color:aliceblue; font-weight: bold;" class="dropdown-item" href="index.php?pid=<?php echo base64_encode("ui/excepcion/searchExcepcion.php") ?>">&nbsp&nbsp&nbsp&nbspExcepcion</a></li>
                  <li><a style="color:aliceblue; font-weight: bold;" class="dropdown-item" href="index.php?pid=<?php echo base64_encode("ui/excepcionPersonal/searchExcepcionPersonal.php") ?>">&nbsp&nbsp&nbsp&nbspExcepcion Personal</a></li>
            </ul>
            </li>
            </br>
            <li>
               <a id="head" href="#" style="color:aliceblue; font-weight: bold;">&nbsp&nbsp&nbsp<img src="img/login.png" width="32px">&nbsp&nbsp&nbsp&nbspLog</a>
               <ul class="nav-pills nav-stacked" style="list-style-type:none;">
                  <li><a style="color:aliceblue; font-weight: bold;" class="dropdown-item" href="index.php?pid=<?php echo base64_encode("ui/logAdministrator/searchLogAdministrator.php") ?>">&nbsp&nbsp&nbsp&nbspLog Administrator</a></li>
                  <li><a style="color:aliceblue; font-weight: bold;" class="dropdown-item" href="index.php?pid=<?php echo base64_encode("ui/logCoordinador/searchLogCoordinador.php") ?>">&nbsp&nbsp&nbsp&nbspLog Coordinador</a></li>
               </ul>
            </li>
           
            <br/>
            <li>
               <a style="color:aliceblue" class="nav-link" href="index.php?pid=<?php echo base64_encode("ui/administrator/cargaDatos.php") ?>"><img src="img/subir.png" width="32px">&nbsp&nbsp&nbsp&nbspCargar datos</a>
               
            </li>
            <br/>
            

            <li>
               <a id="head" href="index.php?logOut=1">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<img src="img/salida.png" width="34px" data-toggle='tooltip' data-placement='right' title='Salir'></a>
            </li>
            <br>
         </ul>
      </div>
      <!-- Sidebar -->

      <!-- /#sidebar-wrapper -->

      <!-- Page Content -->
      <div id="page-content-wrapper">

         <!-- /#wrapper -->

         <!-- Bootstrap core JavaScript -->
         <script src="vendor/jquery/jquery.min.js"></script>
         <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

         <!-- Menu Toggle Script -->
         <script>
            $("#menu-toggle").click(function(e) {
               e.preventDefault();
               $("#wrapper").toggleClass("toggled");
            });
            $("#menu-toggle-2").click(function(e) {
               e.preventDefault();
               $("#wrapper").toggleClass("toggled-2");
               $('#menu ul').hide();
            });

            function initMenu() {
               $('#menu ul').hide();
               $('#menu ul').children('.current').parent().show();
               //$('#menu ul:first').show();
               $('#menu li a').click(
                  function() {
                     var checkElement = $(this).next();
                     if ((checkElement.is('ul')) && (checkElement.is(':visible'))) {
                        return false;
                     }
                     if ((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
                        $('#menu ul:visible').slideUp('normal');
                        checkElement.slideDown('normal');
                        return false;
                     }
                  }
               );
            }
            $(document).ready(function() {
               initMenu();
            });
         </script>
