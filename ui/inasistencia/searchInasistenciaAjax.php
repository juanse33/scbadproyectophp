<?php
$fecha = "";
$profesor = "";
$estado = "";
if (isset($_GET['fecha'])) {
	$fecha = $_GET['fecha'];
}
if (isset($_GET['profesor'])) {
	$profesor = $_GET['profesor'];
}
if (isset($_GET['estado'])) {
	$estado = $_GET['estado'];
}

$inasistencia = new Inasistencia("", $fecha, $estado, $profesor);
$inasistencias = $inasistencia->search();
$tamaño = count($inasistencias);
if ($tamaño > 0) {
?>

	<script charset="utf-8">
		$(function() {
			$("[data-toggle='tooltip']").tooltip();
		});
	</script>

	<table  class="table table-borderless table-hover " id="table">
		<thead>
			<tr>
				<th></th>
				<th nowrap>Dia</th>
				<th nowrap>Fecha</th>
				<th>Profesor</th>
				<?php if($estado == 2){  ?>
					<th nowrap>Justificado</th>
				<?php } ?>
			</tr>
		</thead>
		</tbody>
		<?php

		$counter = 1;
		foreach ($inasistencias as $currentinasistencia) {
			echo "<tr><td>" . $counter . "</td>";
			$dias = array('Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado');
			$fechas = $dias[date('N', strtotime($currentinasistencia->getFecha()))];
			echo "<td>" . $fechas . "</td>";
			echo "<td>" . $currentinasistencia->getFecha() . "</td>";
			echo "<td>" . $currentinasistencia->getProfesor()->getNombre() . "</td>";
			if($estado == 2){
				echo "<td>";
				if (($currentinasistencia->getEstado()) == 0) {
					echo "<center><img src='img/cancelar.png' width='18px'></center>";
				} else {
					echo "<center><img src='img/comprobado.png' width='18px'></center>";
				}
				echo "</td>";
			}
			echo "</tr>";
			$counter++;
		}
		?>
		</tbody>
	</table>
	</div>
	</div>
	</div>
<?php
} else {
?>
	<center>
		<div class="container">
			<h4>No se encontraron resultados</h4>
		</div>
	</center>
	<br/>
<?php
}
?>

<script>
	$(document).ready(function() {
		$('#dtVerticalScrollExample').DataTable({
			"scrollY": "200px",
			"scrollCollapse": true,
		});
		$('.dataTables_length').addClass('bs-select');
	});
</script>





