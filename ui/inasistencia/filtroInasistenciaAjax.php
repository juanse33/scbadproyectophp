<?php
$opcion = "";
if (isset($_GET['opcion'])) {
	$opcion = $_GET['opcion'];
}
?>

<div class="container">
	<div class="row">
		<?php
		if ($opcion == 1) {
		?>
			<div class="col-md-3 offset-3">
				<input type="date" class="form-control" id="fecha" autocomplete="off" />
			</div>
		<?php
		} else {
		?>
			<div class="col-md-4 offset-2">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<select class="selectpicker" data-live-search="true" id="profesor2" name="profesor">
					<option selected disabled value="0">Seleccione el profesor</option>
					<?php
					$objProfesor = new Profesor();
					$profesors = $objProfesor->selectAll();
					foreach ($profesors as $currentProfesor) {
						echo "<option value='" . $currentProfesor->getIdProfesor() . "'>" . $currentProfesor->getNombre() . "</option>";
					} ?>
				</select>

			</div>
		<?php
		}
		?>

		<div class="col-md-3">
			<select class="form-control" name="estado" id="estado">
				<option value="0">No justificadas</option>
				<option value="1">Justificadas</option>
				<option selected option value="2">Todos</option>
			</select>
		</div>
	</div>
	<br/>
	<div class="row">
		<div class="offset-5">
			<button type="submit" class="btn btn-outline-secondary" style="width:10em;" id="buscar" name="buscar">Buscar</button>
		</div>
	</div>
	<br>
	<div class="table-responsive">
	<div id="searchResult"></div>
	</div>
</div>

<script> 
$(document).ready(function() {
$('.selectpicker').selectpicker();
     });
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$("#buscar").click(function() {
				if($("#opcion").val()==2){
				$('#boton2').html('<a href="pdfInasistencia.php?profesor=' + $("#profesor2").val() +'&estado='+$("#estado").val()+'" target="_onblack" data-toggle="tooltip" data-placement="right" class="tooltipLink" data-original-title="Generar pdf"><img src="img/pdf1.png" width="32px"></a>');
			}
		});
		$("#buscar").click(function() {
			var path = "indexAjax.php?pid=<?php echo base64_encode("ui/inasistencia/searchInasistenciaAjax.php"); ?>&estado=" + $("#estado").val() + "&profesor=" + $("#profesor2").val() + "&fecha=" + $("#fecha").val();
			$("#searchResult").load(path);
		});
	});
</script>