<?php
if(isset($_GET['limit'])){
    $limit =  $_GET['limit'];
}
$nombres_json = json_encode($nombres, JSON_UNESCAPED_UNICODE);
$totales_json = json_encode($totales);

?>

<div id="graficaAsignaturas"></div>

<script>
    function crearCadena(json) {
        var parsed = JSON.parse(json);
        var arr = [];
        for (var x in parsed) {
            arr.push(parsed[x]);
        }
        return arr;
    }

    datosX = crearCadena('<?php echo $nombres_json ?>')
    datosY = crearCadena('<?php echo $totales_json ?>')

    var data = [{
        values: datosY,
        labels: datosX, 
        type: 'pie'
    }];


    var config = {responsive: true}
    Plotly.newPlot('graficaAsignaturas', data, '', config);
</script>