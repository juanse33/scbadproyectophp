<?php
require("business/Administrator.php");
require("business/LogAdministrator.php");
require("business/Profesor.php");
require("business/Tipo.php");
require("business/Inscripcion.php");
require("business/Horario.php");
require("business/Asignatura.php");
require("business/Grupo.php");
require("business/Asistencia.php");
require("business/ExcepcionPersonal.php");
require("business/Excepcion.php");
require("business/LogCoordinador.php");
require("business/Coordinador.php");
require("business/Inasistencia.php");
require_once("persistence/Connection.php");

$profesor = "";
if (isset($_GET['idProfesor'])) {
	$profesor = $_GET['idProfesor'];
}
$total = 0;
$objProfesor = new Profesor($profesor);
$objProfesor->select();
$asistencia = new Asistencia("", "", $profesor);
$asistencias = $asistencia->search();
$inasistencia = new Inasistencia("", "", 2, $profesor);
$inasistencias = $inasistencia->search();
$total = $total + count($inasistencias);
$total =  count($inasistencias) + count($asistencias);
$porAsis = (count($asistencias) * 100) / $total;
$porInasis = (count($inasistencias) * 100) / $total;


$inasJus = new Inasistencia("", "", 1, $profesor);
$arrayJus = $inasJus->search();
$inasInjus = new Inasistencia("", "", 0, $profesor);
$arrayInjus = $inasInjus->search();
$porJus = (count($arrayJus) * 100) / count($inasistencias);
$porInjus = (count($arrayInjus) * 100) / count($inasistencias);

?>

<head>
	<script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
	<script src="lib/plotly-latest.min.js"></script>
</head>
<div class="modal-header">
	<h4 class="modal-title">Profesor:<?php echo $objProfesor->getNombre() ?></h4>
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="container">
	<div class="modal-body">
		<div class="row">
			<div class="col">
				<div id="grafica"></div>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<div id="grafica2"></div>
			</div>
		</div>
	</div>
</div>
<script>
	var data = [{
		values: [<?php echo $porAsis; ?>, <?php echo $porInasis; ?>],
		labels: ['Asistenica', 'Inasistencia'],
		type: 'pie'
	}];

	var layout = {
		title: 'Profesor: <?php echo $objProfesor->getNombre() ?>',
		height: 400,
		width: 500
	};

	Plotly.newPlot('grafica', data, layout);
</script>
<script>
	var data = [{
		values: [<?php echo $porJus; ?>, <?php echo $porInjus; ?>],
		labels: ['Inasistencias Justificadas', 'Inasistencias Injustificadas'],
		type: 'pie',
		marker: {
			colors: 'rgb(56, 75, 126)'
		}

	}];

	var layout = {
		title: 'Profesor: <?php echo $objProfesor->getNombre() ?>',
		height: 400,
		width: 500
	};

	Plotly.newPlot('grafica2', data, layout);
</script>