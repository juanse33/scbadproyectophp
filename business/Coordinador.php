<?php
require_once ("persistence/CoordinadorDAO.php");
require_once ("persistence/Connection.php");

class Coordinador {
	private $idCoordinador;
	private $name;
	private $lastname;
	private $email;
	private $password;
	private $picture;
	private $state;
	private $coordinadorDAO;
	private $connection;

	function getIdCoordinador() {
		return $this -> idCoordinador;
	}

	function setIdCoordinador($pIdCoordinador) {
		$this -> idCoordinador = $pIdCoordinador;
	}

	function getName() {
		return $this -> name;
	}

	function setName($pName) {
		$this -> name = $pName;
	}

	function getLastname() {
		return $this -> lastname;
	}

	function setLastname($pLastname) {
		$this -> lastname = $pLastname;
	}

	function getEmail() {
		return $this -> email;
	}

	function setEmail($pEmail) {
		$this -> email = $pEmail;
	}

	function getPassword() {
		return $this -> password;
	}

	function setPassword($pPassword) {
		$this -> password = $pPassword;
	}

	function getPicture() {
		return $this -> picture;
	}

	function setPicture($pPicture) {
		$this -> picture = $pPicture;
	}

	function getState() {
		return $this -> state;
	}

	function setState($pState) {
		$this -> state = $pState;
	}

	function Coordinador($pIdCoordinador = "", $pName = "", $pLastname = "", $pEmail = "", $pPassword = "", $pPicture = "", $pState = ""){
		$this -> idCoordinador = $pIdCoordinador;
		$this -> name = $pName;
		$this -> lastname = $pLastname;
		$this -> email = $pEmail;
		$this -> password = $pPassword;
		$this -> picture = $pPicture;
		$this -> state = $pState;
		$this -> coordinadorDAO = new CoordinadorDAO($this -> idCoordinador, $this -> name, $this -> lastname, $this -> email, $this -> password, $this -> picture, $this -> state);
		$this -> connection = new Connection();
	}

	function logIn($email, $password){
		$this -> connection -> open();
		$this -> connection -> run($this -> coordinadorDAO -> logIn($email, $password));
		if($this -> connection -> numRows()==1){
			$result = $this -> connection -> fetchRow();
			$this -> idCoordinador = $result[0];
			$this -> name = $result[1];
			$this -> lastname = $result[2];
			$this -> email = $result[3];
			$this -> password = $result[4];
			$this -> picture = $result[5];
			$this -> state = $result[6];
			$this -> connection -> close();
			return true;
		}else{
			$this -> connection -> close();
			return false;
		}
	}

	function insert(){
		$this -> connection -> open();
		$this -> connection -> run($this -> coordinadorDAO -> insert());
		$this -> connection -> close();
	}

	function update(){
		$this -> connection -> open();
		$this -> connection -> run($this -> coordinadorDAO -> update());
		$this -> connection -> close();
	}

	function updatePassword($password){
		$this -> connection -> open();
		$this -> connection -> run($this -> coordinadorDAO -> updatePassword($password));
		$this -> connection -> close();
	}

	function existEmail($email){
		$this -> connection -> open();
		$this -> connection -> run($this -> coordinadorDAO -> existEmail($email));
		if($this -> connection -> numRows()==1){
			$this -> connection -> close();
			return true;
		}else{
			$this -> connection -> close();
			return false;
		}
	}

	function recoverPassword($email, $password){
		$this -> connection -> open();
		$this -> connection -> run($this -> coordinadorDAO -> recoverPassword($email, $password));
		$this -> connection -> close();
	}

	function select(){
		$this -> connection -> open();
		$this -> connection -> run($this -> coordinadorDAO -> select());
		$result = $this -> connection -> fetchRow();
		$this -> connection -> close();
		$this -> idCoordinador = $result[0];
		$this -> name = $result[1];
		$this -> lastname = $result[2];
		$this -> email = $result[3];
		$this -> password = $result[4];
		$this -> picture = $result[5];
		$this -> state = $result[6];
	}

	function selectAll(){
		$this -> connection -> open();
		$this -> connection -> run($this -> coordinadorDAO -> selectAll());
		$coordinadors = array();
		while ($result = $this -> connection -> fetchRow()){
			array_push($coordinadors, new Coordinador($result[0], $result[1], $result[2], $result[3], $result[4], $result[5], $result[6]));
		}
		$this -> connection -> close();
		return $coordinadors;
	}

	function selectAllOrder($order, $dir){
		$this -> connection -> open();
		$this -> connection -> run($this -> coordinadorDAO -> selectAllOrder($order, $dir));
		$coordinadors = array();
		while ($result = $this -> connection -> fetchRow()){
			array_push($coordinadors, new Coordinador($result[0], $result[1], $result[2], $result[3], $result[4], $result[5], $result[6]));
		}
		$this -> connection -> close();
		return $coordinadors;
	}

	function search($search){
		$this -> connection -> open();
		$this -> connection -> run($this -> coordinadorDAO -> search($search));
		$coordinadors = array();
		while ($result = $this -> connection -> fetchRow()){
			array_push($coordinadors, new Coordinador($result[0], $result[1], $result[2], $result[3], $result[4], $result[5], $result[6]));
		}
		$this -> connection -> close();
		return $coordinadors;
	}
}
?>
