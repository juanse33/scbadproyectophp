<?php
require_once("persistence/ProfesorDAO.php");
require_once("persistence/Connection.php");
date_default_timezone_set('America/Bogota');
class Profesor
{
	private $idProfesor;
	private $nombre;
	private $correo;
	private $huella;
	private $profesorDAO;
	private $connection;

	function getIdProfesor()
	{
		return $this->idProfesor;
	}

	function setIdProfesor($pIdProfesor)
	{
		$this->idProfesor = $pIdProfesor;
	}

	function getNombre()
	{
		return $this->nombre;
	}

	function setNombre($pNombre)
	{
		$this->nombre = $pNombre;
	}

	
	function getCorreo()
	{
		return $this->correo;
	}

	function setCorreo($pCorreo)
	{
		$this->correo = $pCorreo;
	}

	function getHuella()
	{
		return $this->huella;
	}

	function setHuella($pHuella)
	{
		$this->huella = $pHuella;
	}

	function Profesor($pIdProfesor = "", $pNombre = "", $pCorreo = "", $pHuella = "")
	{
		$this->idProfesor = $pIdProfesor;
		$this->nombre = $pNombre;
		$this->correo = $pCorreo;
		$this->huella = $pHuella;
		$this->profesorDAO = new ProfesorDAO($this->idProfesor, $this->nombre, $this->correo, $this->huella);
		$this->connection = new Connection();
	}

	function insert()
	{
		$this->connection->open();
		$this->connection->run($this->profesorDAO->insert());
		$this->connection->close();
	}

	function update()
	{
		$this->connection->open();
		$this->connection->run($this->profesorDAO->update());
		$this->connection->close();
	}

	function select()
	{
		$this->connection->open();
		$this->connection->run($this->profesorDAO->select());
		$result = $this->connection->fetchRow();
		$this->connection->close();
		$this->idProfesor = $result[0];
		$this->nombre = $result[1];
		$this->correo = $result[2];
		$this->huella = $result[3];
	}
	function selectName()
	{
		$this->connection->open();
		$this->connection->run($this->profesorDAO->selectName());
		$result = $this->connection->fetchRow();
		$this->connection->close();
		$this->idProfesor = $result[0];
		$this->nombre = $result[1];
		$this->correo = $result[2];
		$this->huella = $result[3];
	}
	function selectAll()
	{
		$this->connection->open();
		$this->connection->run($this->profesorDAO->selectAll());
		$profesors = array();
		while ($result = $this->connection->fetchRow()) {
			array_push($profesors, new Profesor($result[0], $result[1], $result[2], $result[3]));
		}
		$this->connection->close();
		return $profesors;
	}
	function selectProfesorByDay($fecha)
	{
		$dias = array('Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado');
		$this->connection->open();
		if (date('N', strtotime($fecha)) == 7) {
			$this->connection->run($this->profesorDAO->selectProfesorByDay("Sabado"));
		} else {
			$dia = $dias[date('N', strtotime($fecha))];
			$this->connection->run($this->profesorDAO->selectProfesorByDay($dia));
		}
		$profesors = array();
		while ($result = $this->connection->fetchRow()) {
			array_push($profesors, new Profesor($result[0]));
		}
		$this->connection->close();
		return $profesors;
	}
	function selectProfesorByAsistencia($fecha)
	{
		$this->connection->open();
		$this->connection->run($this->profesorDAO->selectProfesorByAsistencia($fecha));
		$profesors = array();
		while ($result = $this->connection->fetchRow()) {
			array_push($profesors, $result[0]);
		}
		$this->connection->close();
		return $profesors;
	}

	function selectAllOrder($order, $dir)
	{
		$this->connection->open();
		$this->connection->run($this->profesorDAO->selectAllOrder($order, $dir));
		$profesors = array();
		while ($result = $this->connection->fetchRow()) {
			array_push($profesors, new Profesor($result[0], $result[1], $result[2], $result[3]));
		}
		$this->connection->close();
		return $profesors;
	}

	function search($search)
	{
		$this->connection->open();
		$this->connection->run($this->profesorDAO->search($search));
		$profesors = array();
		while ($result = $this->connection->fetchRow()) {
			array_push($profesors, new Profesor($result[0], $result[1], $result[2], $result[3]));
		}
		$this->connection->close();
		return $profesors;
	}
}
