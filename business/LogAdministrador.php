<?php
require_once ("persistence/LogAdministradorDAO.php");
require_once ("persistence/Connection.php");

class LogAdministrador {
	private $idLogAdministrador;
	private $action;
	private $information;
	private $date;
	private $time;
	private $ip;
	private $os;
	private $browser;
	private $administrador;
	private $logAdministradorDAO;
	private $connection;

	function getIdLogAdministrador() {
		return $this -> idLogAdministrador;
	}

	function setIdLogAdministrador($pIdLogAdministrador) {
		$this -> idLogAdministrador = $pIdLogAdministrador;
	}

	function getAction() {
		return $this -> action;
	}

	function setAction($pAction) {
		$this -> action = $pAction;
	}

	function getInformation() {
		return $this -> information;
	}

	function setInformation($pInformation) {
		$this -> information = $pInformation;
	}

	function getDate() {
		return $this -> date;
	}

	function setDate($pDate) {
		$this -> date = $pDate;
	}

	function getTime() {
		return $this -> time;
	}

	function setTime($pTime) {
		$this -> time = $pTime;
	}

	function getIp() {
		return $this -> ip;
	}

	function setIp($pIp) {
		$this -> ip = $pIp;
	}

	function getOs() {
		return $this -> os;
	}

	function setOs($pOs) {
		$this -> os = $pOs;
	}

	function getBrowser() {
		return $this -> browser;
	}

	function setBrowser($pBrowser) {
		$this -> browser = $pBrowser;
	}

	function getAdministrador() {
		return $this -> administrador;
	}

	function setAdministrador($pAdministrador) {
		$this -> administrador = $pAdministrador;
	}

	function LogAdministrador($pIdLogAdministrador = "", $pAction = "", $pInformation = "", $pDate = "", $pTime = "", $pIp = "", $pOs = "", $pBrowser = "", $pAdministrador = ""){
		$this -> idLogAdministrador = $pIdLogAdministrador;
		$this -> action = $pAction;
		$this -> information = $pInformation;
		$this -> date = $pDate;
		$this -> time = $pTime;
		$this -> ip = $pIp;
		$this -> os = $pOs;
		$this -> browser = $pBrowser;
		$this -> administrador = $pAdministrador;
		$this -> logAdministradorDAO = new LogAdministradorDAO($this -> idLogAdministrador, $this -> action, $this -> information, $this -> date, $this -> time, $this -> ip, $this -> os, $this -> browser, $this -> administrador);
		$this -> connection = new Connection();
	}

	function insert(){
		$this -> connection -> open();
		$this -> connection -> run($this -> logAdministradorDAO -> insert());
		$this -> connection -> close();
	}

	function update(){
		$this -> connection -> open();
		$this -> connection -> run($this -> logAdministradorDAO -> update());
		$this -> connection -> close();
	}

	function select(){
		$this -> connection -> open();
		$this -> connection -> run($this -> logAdministradorDAO -> select());
		$result = $this -> connection -> fetchRow();
		$this -> connection -> close();
		$this -> idLogAdministrador = $result[0];
		$this -> action = $result[1];
		$this -> information = $result[2];
		$this -> date = $result[3];
		$this -> time = $result[4];
		$this -> ip = $result[5];
		$this -> os = $result[6];
		$this -> browser = $result[7];
		$administrador = new Administrador($result[8]);
		$administrador -> select();
		$this -> administrador = $administrador;
	}

	function selectAll(){
		$this -> connection -> open();
		$this -> connection -> run($this -> logAdministradorDAO -> selectAll());
		$logAdministradors = array();
		while ($result = $this -> connection -> fetchRow()){
			$administrador = new Administrador($result[8]);
			$administrador -> select();
			array_push($logAdministradors, new LogAdministrador($result[0], $result[1], $result[2], $result[3], $result[4], $result[5], $result[6], $result[7], $administrador));
		}
		$this -> connection -> close();
		return $logAdministradors;
	}

	function selectAllByAdministrador(){
		$this -> connection -> open();
		$this -> connection -> run($this -> logAdministradorDAO -> selectAllByAdministrador());
		$logAdministradors = array();
		while ($result = $this -> connection -> fetchRow()){
			$administrador = new Administrador($result[8]);
			$administrador -> select();
			array_push($logAdministradors, new LogAdministrador($result[0], $result[1], $result[2], $result[3], $result[4], $result[5], $result[6], $result[7], $administrador));
		}
		$this -> connection -> close();
		return $logAdministradors;
	}

	function selectAllOrder($order, $dir){
		$this -> connection -> open();
		$this -> connection -> run($this -> logAdministradorDAO -> selectAllOrder($order, $dir));
		$logAdministradors = array();
		while ($result = $this -> connection -> fetchRow()){
			$administrador = new Administrador($result[8]);
			$administrador -> select();
			array_push($logAdministradors, new LogAdministrador($result[0], $result[1], $result[2], $result[3], $result[4], $result[5], $result[6], $result[7], $administrador));
		}
		$this -> connection -> close();
		return $logAdministradors;
	}

	function selectAllByAdministradorOrder($order, $dir){
		$this -> connection -> open();
		$this -> connection -> run($this -> logAdministradorDAO -> selectAllByAdministradorOrder($order, $dir));
		$logAdministradors = array();
		while ($result = $this -> connection -> fetchRow()){
			$administrador = new Administrador($result[8]);
			$administrador -> select();
			array_push($logAdministradors, new LogAdministrador($result[0], $result[1], $result[2], $result[3], $result[4], $result[5], $result[6], $result[7], $administrador));
		}
		$this -> connection -> close();
		return $logAdministradors;
	}

	function search($search){
		$this -> connection -> open();
		$this -> connection -> run($this -> logAdministradorDAO -> search($search));
		$logAdministradors = array();
		while ($result = $this -> connection -> fetchRow()){
			$administrador = new Administrador($result[8]);
			$administrador -> select();
			array_push($logAdministradors, new LogAdministrador($result[0], $result[1], $result[2], $result[3], $result[4], $result[5], $result[6], $result[7], $administrador));
		}
		$this -> connection -> close();
		return $logAdministradors;
	}
}
?>
