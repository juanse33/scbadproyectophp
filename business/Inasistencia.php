<?php
require_once ("persistence/InasistenciaDAO.php");
require_once ("persistence/Connection.php");
date_default_timezone_set('America/Bogota');
class Inasistencia {
	private $idInasistencia;
	private $fecha;
	private $estado;
	private $profesor;
	private $inasistenciaDAO;
	private $connection;

	function getIdInasistencia() {
		return $this -> idInasistencia;
	}

	function setIdInasistencia($pIdInasistencia) {
		$this -> idInasistencia = $pIdInasistencia;
	}

	function getFecha() {
		return $this -> fecha;
	}

	function setFecha($pFecha) {
		$this -> fecha = $pFecha;
	}

	function getEstado() {
		return $this -> estado;
	}

	function setEstado($pEstado) {
		$this -> estado = $pEstado;
	}

	function getProfesor() {
		return $this -> profesor;
	}

	function setProfesor($pProfesor) {
		$this -> profesor = $pProfesor;
	}

	function Inasistencia($pIdInasistencia = "", $pFecha = "", $pEstado = "", $pProfesor = ""){
		$this -> idInasistencia = $pIdInasistencia;
		$this -> fecha = $pFecha;
		$this -> estado = $pEstado;
		$this -> profesor = $pProfesor;
		$this -> inasistenciaDAO = new InasistenciaDAO($this -> idInasistencia, $this -> fecha, $this -> estado, $this -> profesor);
		$this -> connection = new Connection();
	}

	function insert(){
		$fechaA = date('Y-m-d');
		$fech = '' . date("Y-m-d", strtotime('-1 day', strtotime($fechaA)));
		if (date('N', strtotime($fech)) == 7)
			$fech = '' . date("Y-m-d", strtotime('-2 day', strtotime($fechaA)));
		$this -> connection -> open();
		$this -> connection -> run($this -> inasistenciaDAO -> insert($fech));
		$this -> connection -> close();
	}

	function update(){
		$this -> connection -> open();
		$this -> connection -> run($this -> inasistenciaDAO -> update());
		$this -> connection -> close();
	}
	function updateExcepcion(){
		$this -> connection -> open();
		$this -> connection -> run($this -> inasistenciaDAO -> updateExcepcion());
		$this -> connection -> close();
	}

	function select(){
		$this -> connection -> open();
		$this -> connection -> run($this -> inasistenciaDAO -> select());
		$result = $this -> connection -> fetchRow();
		$this -> connection -> close();
		$this -> idInasistencia = $result[0];
		$this -> fecha = $result[1];
		$this -> estado = $result[2];
		$profesor = new Profesor($result[3]);
		$profesor -> select();
		$this -> profesor = $profesor;
	}

	function selectAll(){
		$this -> connection -> open();
		$this -> connection -> run($this -> inasistenciaDAO -> selectAll());
		$inasistencias = array();
		while ($result = $this -> connection -> fetchRow()){
			$profesor = new Profesor($result[3]);
			$profesor -> select();
			array_push($inasistencias, new Inasistencia($result[0], $result[1], $result[2], $profesor));
		}
		$this -> connection -> close();
		return $inasistencias;
	}

	function selectAllByProfesor(){
		$this -> connection -> open();
		$this -> connection -> run($this -> inasistenciaDAO -> selectAllByProfesor());
		$inasistencias = array();
		while ($result = $this -> connection -> fetchRow()){
			$profesor = new Profesor($result[3]);
			$profesor -> select();
			array_push($inasistencias, new Inasistencia($result[0], $result[1], $result[2], $profesor));
		}
		$this -> connection -> close();
		return $inasistencias;
	}

	function selectAllOrder($order, $dir){
		$this -> connection -> open();
		$this -> connection -> run($this -> inasistenciaDAO -> selectAllOrder($order, $dir));
		$inasistencias = array();
		while ($result = $this -> connection -> fetchRow()){
			$profesor = new Profesor($result[3]);
			$profesor -> select();
			array_push($inasistencias, new Inasistencia($result[0], $result[1], $result[2], $profesor));
		}
		$this -> connection -> close();
		return $inasistencias;
	}

	function selectAllByProfesorOrder($order, $dir){
		$this -> connection -> open();
		$this -> connection -> run($this -> inasistenciaDAO -> selectAllByProfesorOrder($order, $dir));
		$inasistencias = array();
		while ($result = $this -> connection -> fetchRow()){
			$profesor = new Profesor($result[3]);
			$profesor -> select();
			array_push($inasistencias, new Inasistencia($result[0], $result[1], $result[2], $profesor));
		}
		$this -> connection -> close();
		return $inasistencias;
	}

	function search(){
		$this -> connection -> open();
        if ($this->estado == 2) {
			$this -> connection -> run($this -> inasistenciaDAO -> search2());
        }
		else{
			$this -> connection -> run($this -> inasistenciaDAO -> search());
		}
			
		$asistencias = array();
		while ($result = $this -> connection -> fetchRow()){
			$profesor = new Profesor($result[3]);
			$profesor -> select();
			array_push($asistencias, new Inasistencia($result[0], $result[1], $result[2],$profesor));
		}
		$this -> connection -> close();
		return $asistencias;
	}
}
?>
