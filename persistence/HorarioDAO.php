<?php
class HorarioDAO{
	private $idHorario;
	private $dia;
	private $hora;
	private $inscripcion;

	function HorarioDAO($pIdHorario = "", $pDia = "", $pHora = "", $pInscripcion = ""){
		$this -> idHorario = $pIdHorario;
		$this -> dia = $pDia;
		$this -> hora = $pHora;
		$this -> inscripcion = $pInscripcion;
	}

	function insert(){
		return "insert into horario(dia, hora, inscripcion_idInscripcion)
				values('" . $this -> dia . "', '" . $this -> hora . "', '" . $this -> inscripcion . "')";
	}

	function update(){
		return "update horario set 
				dia = '" . $this -> dia . "',
				hora = '" . $this -> hora . "',
				inscripcion_idInscripcion = '" . $this -> inscripcion . "'	
				where idHorario = '" . $this -> idHorario . "'";
	}

	function select() {
		return "select idHorario, dia, hora, inscripcion_idInscripcion
				from horario
				where idHorario = '" . $this -> idHorario . "'";
	}

	function selectAll() {
		return "select idHorario, dia, hora, inscripcion_idInscripcion
				from horario";
	}

	function selectAllByInscripcion() {
		return "select idHorario, dia, hora, inscripcion_idInscripcion
				from horario
				where inscripcion_idInscripcion = '" . $this -> inscripcion . "'";
	}

	function selectAllOrder($orden, $dir){
		return "select idHorario, dia, hora, inscripcion_idInscripcion
				from horario
				order by " . $orden . " " . $dir;
	}

	function selectAllByInscripcionOrder($orden, $dir) {
		return "select idHorario, dia, hora, inscripcion_idInscripcion
				from horario
				where inscripcion_idInscripcion = '" . $this -> inscripcion . "'
				order by " . $orden . " " . $dir;
	}

	function search($search) {
		return "select idHorario, dia, hora, inscripcion_idInscripcion
				from horario
				where dia like '%" . $search . "%' or hora like '%" . $search . "%'";
	}
}
?>
