<?php
class TipoDAO{
	private $idTipo;
	private $nombre;

	function TipoDAO($pIdTipo = "", $pNombre = ""){
		$this -> idTipo = $pIdTipo;
		$this -> nombre = $pNombre;
	}

	function insert(){
		return "insert into tipo(nombre)
				values('" . $this -> nombre . "')";
	}

	function update(){
		return "update tipo set 
				nombre = '" . $this -> nombre . "'	
				where idTipo = '" . $this -> idTipo . "'";
	}

	function select() {
		return "select idTipo, nombre
				from tipo
				where idTipo = '" . $this -> idTipo . "'";
	}

	function selectAll() {
		return "select idTipo, nombre
				from tipo";
	}

	function selectAllOrder($orden, $dir){
		return "select idTipo, nombre
				from tipo
				order by " . $orden . " " . $dir;
	}

	function search($search) {
		return "select idTipo, nombre
				from tipo
				where nombre like '%" . $search . "%'";
	}
}
?>
