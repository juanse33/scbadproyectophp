<?php
class ProfesorDAO{
	private $idProfesor;
	private $nombre;
	private $correo;
	private $huella;

	function ProfesorDAO($pIdProfesor = "", $pNombre = "", $pCorreo = "", $pHuella = ""){
		$this -> idProfesor = $pIdProfesor;
		$this -> nombre = $pNombre;
		$this -> correo = $pCorreo;
		$this -> huella = $pHuella;
	}

	function insert(){
		return "insert into profesor(nombre,  correo, huella)
				values('" . $this -> nombre . "','" . $this -> correo . "', '" . $this -> huella . "')";
	}

	function update(){
		return "update profesor set 
				nombre = '" . $this -> nombre . "',
				correo = '" . $this -> correo . "',
				huella = '" . $this -> huella . "'	
				where idProfesor = '" . $this -> idProfesor . "'";
	}

	function select() {
		return "select idProfesor, nombre, correo, huella
				from profesor
				where idProfesor = '" . $this -> idProfesor . "'";
	}
	function selectName() {
		return "select idProfesor, nombre, correo, huella
				from profesor
				where nombre = '" . $this -> nombre . "'";
	}

	function selectAll() {
		return "select idProfesor, nombre, correo, huella
				from profesor";
	}
	function selectProfesorByDay($dia){
		return "select idProfesor from profesor where idProfesor in (select profesor_idProfesor from inscripcion	
				where idInscripcion	in( select inscripcion_idInscripcion from horario where dia = '$dia'))";
	}
	function selectProfesorByAsistencia($fecha){
		return "select profesor_idProfesor from asistencia where fecha = '$fecha'";
	}
	function selectAllOrder($orden, $dir){
		return "select idProfesor, nombre,  correo, huella
				from profesor
				order by " . $orden . " " . $dir;
	}

	function search($search) {
		return "select idProfesor, nombre, correo, huella
				from profesor
				where nombre like '%" . $search . "%' or correo like '%" . $search . "%' or huella like '%" . $search . "%'";
	}
}
?>
