<?php
class InasistenciaDAO{
	private $idInasistencia;
	private $fecha;
	private $estado;
	private $profesor;

	function InasistenciaDAO($pIdInasistencia = "", $pFecha = "", $pEstado = "", $pProfesor = ""){
		$this -> idInasistencia = $pIdInasistencia;
		$this -> fecha = $pFecha;
		$this -> estado = $pEstado;
		$this -> profesor = $pProfesor;
	}

	function insert($fech){
		return "insert into inasistencia(fecha, estado, profesor_idProfesor)
				values('" . $fech . "', '" . $this -> estado . "', '" . $this -> profesor . "')";
	}

	function update(){
		return "update inasistencia set 
				fecha = '" . $this -> fecha . "',
				estado = '" . $this -> estado . "',
				profesor_idProfesor = '" . $this -> profesor . "'	
				where idInasistencia = '" . $this -> idInasistencia . "'";
	}
	function updateExcepcion(){
		return "update inasistencia set 
				estado = '1'	
				where fecha = '" . $this -> fecha . "' and
				profesor_idProfesor = '" . $this -> profesor . "'";
	}

	function select() {
		return "select idInasistencia, fecha, estado, profesor_idProfesor
				from inasistencia
				where idInasistencia = '" . $this -> idInasistencia . "'";
	}

	function selectAll() {
		return "select idInasistencia, fecha, estado, profesor_idProfesor
				from inasistencia";
	}

	function selectAllByProfesor() {
		return "select idInasistencia, fecha, estado, profesor_idProfesor
				from inasistencia
				where profesor_idProfesor = '" . $this -> profesor . "'";
	}

	function selectAllOrder($orden, $dir){
		return "select idInasistencia, fecha, estado, profesor_idProfesor
				from inasistencia
				order by " . $orden . " " . $dir;
	}

	function selectAllByProfesorOrder($orden, $dir) {
		return "select idInasistencia, fecha, estado, profesor_idProfesor
				from inasistencia
				where profesor_idProfesor = '" . $this -> profesor . "'
				order by " . $orden . " " . $dir;
	}

	function search() {
		return "select * from inasistencia where (fecha='". $this -> fecha ."' or profesor_idProfesor='". $this -> profesor ."') and estado ='" . $this -> estado . "'";
	}
	function search2() {
		return "select * from inasistencia where fecha='". $this -> fecha ."' or profesor_idProfesor='". $this -> profesor ."'";
	}
}
?>
