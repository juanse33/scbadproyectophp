<?php
class GrupoDAO{
	private $idGrupo;
	private $asignatura;
	private $inscripcion;

	function GrupoDAO($pIdGrupo = "", $pAsignatura = "", $pInscripcion = ""){
		$this -> idGrupo = $pIdGrupo;
		$this -> asignatura = $pAsignatura;
		$this -> inscripcion = $pInscripcion;
	}

	function insert(){
		return "insert into grupo(idGrupo,asignatura_idAsignatura, inscripcion_idInscripcion)
				values('" . $this -> idGrupo . "','" . $this -> asignatura . "', '" . $this -> inscripcion . "')";
	}

	function update(){
		return "update grupo set 
				asignatura_idAsignatura = '" . $this -> asignatura . "',
				inscripcion_idInscripcion = '" . $this -> inscripcion . "'	
				where idGrupo = '" . $this -> idGrupo . "'";
	}

	function select() {
		return "select idGrupo, asignatura_idAsignatura, inscripcion_idInscripcion
				from grupo
				where idGrupo = '" . $this -> idGrupo . "'";
	}

	function selectAll() {
		return "select idGrupo, asignatura_idAsignatura, inscripcion_idInscripcion
				from grupo";
	}

	function selectAllByAsignatura() {
		return "select idGrupo, asignatura_idAsignatura, inscripcion_idInscripcion
				from grupo
				where asignatura_idAsignatura = '" . $this -> asignatura . "'";
	}

	function selectAllByInscripcion() {
		return "select idGrupo, asignatura_idAsignatura, inscripcion_idInscripcion
				from grupo
				where inscripcion_idInscripcion = '" . $this -> inscripcion . "'";
	}

	function selectAllOrder($orden, $dir){
		return "select idGrupo, asignatura_idAsignatura, inscripcion_idInscripcion
				from grupo
				order by " . $orden . " " . $dir;
	}

	function selectAllByAsignaturaOrder($orden, $dir) {
		return "select idGrupo, asignatura_idAsignatura, inscripcion_idInscripcion
				from grupo
				where asignatura_idAsignatura = '" . $this -> asignatura . "'
				order by " . $orden . " " . $dir;
	}

	function selectAllByInscripcionOrder($orden, $dir) {
		return "select idGrupo, asignatura_idAsignatura, inscripcion_idInscripcion
				from grupo
				where inscripcion_idInscripcion = '" . $this -> inscripcion . "'
				order by " . $orden . " " . $dir;
	}
}
?>
