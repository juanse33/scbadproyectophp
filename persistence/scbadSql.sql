-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 25-07-2020 a las 00:37:18
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `scbad`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrator`
--

CREATE TABLE `administrator` (
  `idAdministrator` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `lastName` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `picture` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `mobile` varchar(45) DEFAULT NULL,
  `state` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asignatura`
--

CREATE TABLE `asignatura` (
  `idAsignatura` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Disparadores `asignatura`
--
DELIMITER $$
CREATE TRIGGER `asignatura` BEFORE INSERT ON `asignatura` FOR EACH ROW BEGIN     DECLARE cont int (11);         select COUNT(*) from asignatura where nombre = NEW.nombre INTO cont;                                                IF (cont = 1) THEN      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "No se puede guardar mas de una vez la asistencia por docente";     END IF; END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asistencia`
--

CREATE TABLE `asistencia` (
  `idAsistencia` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `profesor_idProfesor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Disparadores `asistencia`
--
DELIMITER $$
CREATE TRIGGER `asistenciaProfesor` BEFORE INSERT ON `asistencia` FOR EACH ROW BEGIN     DECLARE cont int (11);         select COUNT(*) from asistencia where profesor_idProfesor = NEW.profesor_idProfesor AND fecha=NEW.fecha INTO cont;                                                IF (cont = 1) THEN      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "No se puede guardar mas de una vez la asistencia por docente";     END IF; END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coordinador`
--

CREATE TABLE `coordinador` (
  `idCoordinador` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `lastname` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `picture` varchar(45) DEFAULT NULL,
  `state` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `excepcion`
--

CREATE TABLE `excepcion` (
  `idExcepcion` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `fecha` date NOT NULL,
  `tipo_idTipo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `excepcionpersonal`
--

CREATE TABLE `excepcionpersonal` (
  `idExcepcionPersonal` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `fecha` date NOT NULL,
  `profesor_idProfesor` int(11) NOT NULL,
  `tipo_idTipo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupo`
--

CREATE TABLE `grupo` (
  `idGrupo` varchar(10) NOT NULL,
  `asignatura_idAsignatura` int(11) NOT NULL,
  `inscripcion_idInscripcion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Disparadores `grupo`
--
DELIMITER $$
CREATE TRIGGER `insertGrupo` BEFORE INSERT ON `grupo` FOR EACH ROW BEGIN     DECLARE cont int (11);         select COUNT(*) from grupo where asignatura_idAsignatura = NEW.asignatura_idAsignatura AND inscripcion_idInscripcion=NEW.inscripcion_idInscripcion INTO cont;                                                IF (cont = 1) THEN      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "No se puede guardar mas de una vez la asistencia por docente";     END IF; END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horario`
--

CREATE TABLE `horario` (
  `idHorario` int(11) NOT NULL,
  `dia` varchar(45) NOT NULL,
  `hora` varchar(45) NOT NULL,
  `inscripcion_idInscripcion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Disparadores `horario`
--
DELIMITER $$
CREATE TRIGGER `insertHorario` BEFORE INSERT ON `horario` FOR EACH ROW BEGIN     DECLARE cont int (11);         select COUNT(*) from horario where dia = NEW.dia AND inscripcion_idInscripcion=NEW.inscripcion_idInscripcion INTO cont;                                                IF (cont = 1) THEN      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "No se puede guardar mas de una vez la asistencia por docente";     END IF; END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inasistencia`
--

CREATE TABLE `inasistencia` (
  `idInasistencia` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `estado` int(11) NOT NULL,
  `profesor_idProfesor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Disparadores `inasistencia`
--
DELIMITER $$
CREATE TRIGGER `inasistenciaProfesor` BEFORE INSERT ON `inasistencia` FOR EACH ROW BEGIN     DECLARE cont int (11);         select COUNT(*) from inasistencia where profesor_idProfesor = NEW.profesor_idProfesor AND fecha=NEW.fecha INTO cont;                                                IF (cont = 1) THEN      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "No se puede guardar mas de una vez la asistencia por docente";     END IF; END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inscripcion`
--

CREATE TABLE `inscripcion` (
  `idInscripcion` int(11) NOT NULL,
  `periodo` varchar(45) NOT NULL,
  `profesor_idProfesor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Disparadores `inscripcion`
--
DELIMITER $$
CREATE TRIGGER `inscripcion` BEFORE INSERT ON `inscripcion` FOR EACH ROW BEGIN     DECLARE cont int (11);         select COUNT(*) from inscripcion where profesor_idProfesor = NEW.profesor_idProfesor AND periodo=NEW.periodo INTO cont;                                                IF (cont = 1) THEN      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "No se puede guardar mas de una vez la asistencia por docente";     END IF; END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logadministrator`
--

CREATE TABLE `logadministrator` (
  `idLogAdministrator` int(11) NOT NULL,
  `action` varchar(45) NOT NULL,
  `information` text NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `ip` varchar(45) NOT NULL,
  `os` varchar(45) NOT NULL,
  `browser` varchar(45) NOT NULL,
  `administrator_idAdministrator` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logcoordinador`
--

CREATE TABLE `logcoordinador` (
  `idLogCoordinador` int(11) NOT NULL,
  `action` varchar(45) NOT NULL,
  `information` text NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `ip` varchar(45) NOT NULL,
  `os` varchar(45) NOT NULL,
  `browser` varchar(45) NOT NULL,
  `coordinador_idCoordinador` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profesor`
--

CREATE TABLE `profesor` (
  `idProfesor` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `correo` varchar(45) DEFAULT NULL,
  `huella` blob DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Disparadores `profesor`
--
DELIMITER $$
CREATE TRIGGER `insertProfesor` BEFORE INSERT ON `profesor` FOR EACH ROW BEGIN     DECLARE cont int (11);         select COUNT(*) from profesor where nombre = NEW.nombre INTO cont;                                                IF (cont = 1) THEN      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "No se puede guardar mas de una vez la asistencia por docente";     END IF; END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo`
--

CREATE TABLE `tipo` (
  `idTipo` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrator`
--
ALTER TABLE `administrator`
  ADD PRIMARY KEY (`idAdministrator`);

--
-- Indices de la tabla `asignatura`
--
ALTER TABLE `asignatura`
  ADD PRIMARY KEY (`idAsignatura`);

--
-- Indices de la tabla `asistencia`
--
ALTER TABLE `asistencia`
  ADD PRIMARY KEY (`idAsistencia`),
  ADD KEY `profesor_idProfesor` (`profesor_idProfesor`);

--
-- Indices de la tabla `coordinador`
--
ALTER TABLE `coordinador`
  ADD PRIMARY KEY (`idCoordinador`);

--
-- Indices de la tabla `excepcion`
--
ALTER TABLE `excepcion`
  ADD PRIMARY KEY (`idExcepcion`),
  ADD KEY `tipo_idTipo` (`tipo_idTipo`);

--
-- Indices de la tabla `excepcionpersonal`
--
ALTER TABLE `excepcionpersonal`
  ADD PRIMARY KEY (`idExcepcionPersonal`),
  ADD KEY `profesor_idProfesor` (`profesor_idProfesor`),
  ADD KEY `tipo_idTipo` (`tipo_idTipo`);

--
-- Indices de la tabla `grupo`
--
ALTER TABLE `grupo`
  ADD PRIMARY KEY (`idGrupo`),
  ADD KEY `asignatura_idAsignatura` (`asignatura_idAsignatura`),
  ADD KEY `inscripcion_idInscripcion` (`inscripcion_idInscripcion`);

--
-- Indices de la tabla `horario`
--
ALTER TABLE `horario`
  ADD PRIMARY KEY (`idHorario`),
  ADD KEY `inscripcion_idInscripcion` (`inscripcion_idInscripcion`);

--
-- Indices de la tabla `inasistencia`
--
ALTER TABLE `inasistencia`
  ADD PRIMARY KEY (`idInasistencia`),
  ADD KEY `profesor_idProfesor` (`profesor_idProfesor`);

--
-- Indices de la tabla `inscripcion`
--
ALTER TABLE `inscripcion`
  ADD PRIMARY KEY (`idInscripcion`),
  ADD KEY `profesor_idProfesor` (`profesor_idProfesor`);

--
-- Indices de la tabla `logadministrator`
--
ALTER TABLE `logadministrator`
  ADD PRIMARY KEY (`idLogAdministrator`),
  ADD KEY `administrator_idAdministrator` (`administrator_idAdministrator`);

--
-- Indices de la tabla `logcoordinador`
--
ALTER TABLE `logcoordinador`
  ADD PRIMARY KEY (`idLogCoordinador`),
  ADD KEY `coordinador_idCoordinador` (`coordinador_idCoordinador`);

--
-- Indices de la tabla `profesor`
--
ALTER TABLE `profesor`
  ADD PRIMARY KEY (`idProfesor`);

--
-- Indices de la tabla `tipo`
--
ALTER TABLE `tipo`
  ADD PRIMARY KEY (`idTipo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administrator`
--
ALTER TABLE `administrator`
  MODIFY `idAdministrator` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `asistencia`
--
ALTER TABLE `asistencia`
  MODIFY `idAsistencia` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `coordinador`
--
ALTER TABLE `coordinador`
  MODIFY `idCoordinador` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `excepcion`
--
ALTER TABLE `excepcion`
  MODIFY `idExcepcion` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `excepcionpersonal`
--
ALTER TABLE `excepcionpersonal`
  MODIFY `idExcepcionPersonal` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `horario`
--
ALTER TABLE `horario`
  MODIFY `idHorario` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `inasistencia`
--
ALTER TABLE `inasistencia`
  MODIFY `idInasistencia` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `inscripcion`
--
ALTER TABLE `inscripcion`
  MODIFY `idInscripcion` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `logadministrator`
--
ALTER TABLE `logadministrator`
  MODIFY `idLogAdministrator` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `logcoordinador`
--
ALTER TABLE `logcoordinador`
  MODIFY `idLogCoordinador` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `profesor`
--
ALTER TABLE `profesor`
  MODIFY `idProfesor` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tipo`
--
ALTER TABLE `tipo`
  MODIFY `idTipo` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `asistencia`
--
ALTER TABLE `asistencia`
  ADD CONSTRAINT `asistencia_ibfk_1` FOREIGN KEY (`profesor_idProfesor`) REFERENCES `profesor` (`idProfesor`);

--
-- Filtros para la tabla `excepcion`
--
ALTER TABLE `excepcion`
  ADD CONSTRAINT `excepcion_ibfk_1` FOREIGN KEY (`tipo_idTipo`) REFERENCES `tipo` (`idTipo`);

--
-- Filtros para la tabla `excepcionpersonal`
--
ALTER TABLE `excepcionpersonal`
  ADD CONSTRAINT `excepcionpersonal_ibfk_1` FOREIGN KEY (`profesor_idProfesor`) REFERENCES `profesor` (`idProfesor`),
  ADD CONSTRAINT `excepcionpersonal_ibfk_2` FOREIGN KEY (`tipo_idTipo`) REFERENCES `tipo` (`idTipo`);

--
-- Filtros para la tabla `grupo`
--
ALTER TABLE `grupo`
  ADD CONSTRAINT `grupo_ibfk_1` FOREIGN KEY (`asignatura_idAsignatura`) REFERENCES `asignatura` (`idAsignatura`),
  ADD CONSTRAINT `grupo_ibfk_2` FOREIGN KEY (`inscripcion_idInscripcion`) REFERENCES `inscripcion` (`idInscripcion`);

--
-- Filtros para la tabla `horario`
--
ALTER TABLE `horario`
  ADD CONSTRAINT `horario_ibfk_1` FOREIGN KEY (`inscripcion_idInscripcion`) REFERENCES `inscripcion` (`idInscripcion`);

--
-- Filtros para la tabla `inasistencia`
--
ALTER TABLE `inasistencia`
  ADD CONSTRAINT `inasistencia_ibfk_1` FOREIGN KEY (`profesor_idProfesor`) REFERENCES `profesor` (`idProfesor`);

--
-- Filtros para la tabla `inscripcion`
--
ALTER TABLE `inscripcion`
  ADD CONSTRAINT `inscripcion_ibfk_1` FOREIGN KEY (`profesor_idProfesor`) REFERENCES `profesor` (`idProfesor`);

--
-- Filtros para la tabla `logadministrator`
--
ALTER TABLE `logadministrator`
  ADD CONSTRAINT `logadministrator_ibfk_1` FOREIGN KEY (`administrator_idAdministrator`) REFERENCES `administrator` (`idAdministrator`);

--
-- Filtros para la tabla `logcoordinador`
--
ALTER TABLE `logcoordinador`
  ADD CONSTRAINT `logcoordinador_ibfk_1` FOREIGN KEY (`coordinador_idCoordinador`) REFERENCES `coordinador` (`idCoordinador`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
