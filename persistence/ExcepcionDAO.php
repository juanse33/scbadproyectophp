<?php
class ExcepcionDAO{
	private $idExcepcion;
	private $descripcion;
	private $fecha;
	private $tipo;

	function ExcepcionDAO($pIdExcepcion = "", $pDescripcion = "", $pFecha = "", $pTipo = ""){
		$this -> idExcepcion = $pIdExcepcion;
		$this -> descripcion = $pDescripcion;
		$this -> fecha = $pFecha;
		$this -> tipo = $pTipo;
	}

	function insert(){
		return "insert into excepcion(descripcion, fecha, tipo_idTipo)
				values('" . $this -> descripcion . "', '" . $this -> fecha . "', '" . $this -> tipo . "')";
	}

	function update(){
		return "update excepcion set 
				descripcion = '" . $this -> descripcion . "',
				fecha = '" . $this -> fecha . "',
				tipo_idTipo = '" . $this -> tipo . "'	
				where idExcepcion = '" . $this -> idExcepcion . "'";
	}

	function select() {
		return "select idExcepcion, descripcion, fecha, tipo_idTipo
				from excepcion
				where idExcepcion = '" . $this -> idExcepcion . "'";
	}

	function selectAll() {
		return "select idExcepcion, descripcion, fecha, tipo_idTipo
				from excepcion";
	}

	function selectAllByTipo() {
		return "select idExcepcion, descripcion, fecha, tipo_idTipo
				from excepcion
				where tipo_idTipo = '" . $this -> tipo . "'";
	}

	function selectAllOrder($orden, $dir){
		return "select idExcepcion, descripcion, fecha, tipo_idTipo
				from excepcion
				order by " . $orden . " " . $dir;
	}

	function selectAllByTipoOrder($orden, $dir) {
		return "select idExcepcion, descripcion, fecha, tipo_idTipo
				from excepcion
				where tipo_idTipo = '" . $this -> tipo . "'
				order by " . $orden . " " . $dir;
	}

	function search() {
		return "select idExcepcion, descripcion, fecha, tipo_idTipo
				from excepcion
				where tipo_idTipo='" . $this -> tipo . "' or fecha='". $this -> fecha ."'";
	}

	function selectByFecha($fech) {
		return "select idExcepcion, descripcion, fecha, tipo_idTipo
				from excepcion 
				where fecha = '" . $fech . "'";
	}
}
?>
