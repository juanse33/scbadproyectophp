<?php
class LogAdministradorDAO{
	private $idLogAdministrador;
	private $action;
	private $information;
	private $date;
	private $time;
	private $ip;
	private $os;
	private $browser;
	private $administrador;

	function LogAdministradorDAO($pIdLogAdministrador = "", $pAction = "", $pInformation = "", $pDate = "", $pTime = "", $pIp = "", $pOs = "", $pBrowser = "", $pAdministrador = ""){
		$this -> idLogAdministrador = $pIdLogAdministrador;
		$this -> action = $pAction;
		$this -> information = $pInformation;
		$this -> date = $pDate;
		$this -> time = $pTime;
		$this -> ip = $pIp;
		$this -> os = $pOs;
		$this -> browser = $pBrowser;
		$this -> administrador = $pAdministrador;
	}

	function insert(){
		return "insert into LogAdministrador(action, information, date, time, ip, os, browser, administrador_idAdministrador)
				values('" . $this -> action . "', '" . $this -> information . "', '" . $this -> date . "', '" . $this -> time . "', '" . $this -> ip . "', '" . $this -> os . "', '" . $this -> browser . "', '" . $this -> administrador . "')";
	}

	function update(){
		return "update LogAdministrador set 
				action = '" . $this -> action . "',
				information = '" . $this -> information . "',
				date = '" . $this -> date . "',
				time = '" . $this -> time . "',
				ip = '" . $this -> ip . "',
				os = '" . $this -> os . "',
				browser = '" . $this -> browser . "',
				administrador_idAdministrador = '" . $this -> administrador . "'	
				where idLogAdministrador = '" . $this -> idLogAdministrador . "'";
	}

	function select() {
		return "select idLogAdministrador, action, information, date, time, ip, os, browser, administrador_idAdministrador
				from LogAdministrador
				where idLogAdministrador = '" . $this -> idLogAdministrador . "'";
	}

	function selectAll() {
		return "select idLogAdministrador, action, information, date, time, ip, os, browser, administrador_idAdministrador
				from LogAdministrador";
	}

	function selectAllByAdministrador() {
		return "select idLogAdministrador, action, information, date, time, ip, os, browser, administrador_idAdministrador
				from LogAdministrador
				where administrador_idAdministrador = '" . $this -> administrador . "'";
	}

	function selectAllOrder($orden, $dir){
		return "select idLogAdministrador, action, information, date, time, ip, os, browser, administrador_idAdministrador
				from LogAdministrador
				order by " . $orden . " " . $dir;
	}

	function selectAllByAdministradorOrder($orden, $dir) {
		return "select idLogAdministrador, action, information, date, time, ip, os, browser, administrador_idAdministrador
				from LogAdministrador
				where administrador_idAdministrador = '" . $this -> administrador . "'
				order by " . $orden . " " . $dir;
	}

	function search($search) {
		return "select idLogAdministrador, action, information, date, time, ip, os, browser, administrador_idAdministrador
				from LogAdministrador
				where action like '%" . $search . "%' or date like '%" . $search . "%' or time like '%" . $search . "%' or ip like '%" . $search . "%' or os like '%" . $search . "%' or browser like '%" . $search . "%'
				order by date desc, time desc";
	}
}
?>
