<?php
class AsistenciaDAO{
	private $idAsistencia;
	private $fecha;
	private $profesor;

	function AsistenciaDAO($pIdAsistencia = "", $pFecha = "", $pProfesor = ""){
		$this -> idAsistencia = $pIdAsistencia;
		$this -> fecha = $pFecha;
		$this -> profesor = $pProfesor;
	}

	function insert(){
		return "insert into asistencia(fecha, profesor_idProfesor)
				values('" . $this -> fecha . "', '" . $this -> profesor . "')";
	}

	function update(){
		return "update asistencia set 
				fecha = '" . $this -> fecha . "',
				profesor_idProfesor = '" . $this -> profesor . "'	
				where idAsistencia = '" . $this -> idAsistencia . "'";
	}

	function select() {
		return "select idAsistencia, fecha, profesor_idProfesor
				from asistencia
				where idAsistencia = '" . $this -> idAsistencia . "'";
	}

	function selectAll() {
		return "select idAsistencia, fecha, profesor_idProfesor
				from asistencia";
	}

	function selectAllByProfesor() {
		return "select idAsistencia, fecha, profesor_idProfesor
				from asistencia
				where profesor_idProfesor = '" . $this -> profesor . "'";
	}

	function selectAllOrder($orden, $dir){
		return "select idAsistencia, fecha, profesor_idProfesor
				from asistencia
				order by " . $orden . " " . $dir;
	}

	function selectAllByProfesorOrder($orden, $dir) {
		return "select idAsistencia, fecha, profesor_idProfesor
				from asistencia
				where profesor_idProfesor = '" . $this -> profesor . "'
				order by " . $orden . " " . $dir;
	}

	function search() {
		return "select idAsistencia, fecha, profesor_idProfesor from asistencia where fecha='". $this -> fecha ."' or profesor_idProfesor='". $this -> profesor ."'";
	}
}
?>
