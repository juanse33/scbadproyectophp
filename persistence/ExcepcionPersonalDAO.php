<?php
class ExcepcionPersonalDAO{
	private $idExcepcionPersonal;
	private $descripcion;
	private $fecha;
	private $profesor;
	private $tipo;

	function ExcepcionPersonalDAO($pIdExcepcionPersonal = "", $pDescripcion = "", $pFecha = "", $pProfesor = "", $pTipo = ""){
		$this -> idExcepcionPersonal = $pIdExcepcionPersonal;
		$this -> descripcion = $pDescripcion;
		$this -> fecha = $pFecha;
		$this -> profesor = $pProfesor;
		$this -> tipo = $pTipo;
	}

	function insert(){
		return "insert into excepcionpersonal(descripcion, fecha, profesor_idProfesor, tipo_idTipo)
				values('" . $this -> descripcion . "', '" . $this -> fecha . "', '" . $this -> profesor . "', '" . $this -> tipo . "')";
	}

	function update(){
		return "update excepcionpersonal set 
				descripcion = '" . $this -> descripcion . "',
				fecha = '" . $this -> fecha . "',
				profesor_idProfesor = '" . $this -> profesor . "',
				tipo_idTipo = '" . $this -> tipo . "'	
				where idExcepcionPersonal = '" . $this -> idExcepcionPersonal . "'";
	}

	function select() {
		return "select idExcepcionPersonal, descripcion, fecha, profesor_idProfesor, tipo_idTipo
				from excepcionpersonal
				where idExcepcionPersonal = '" . $this -> idExcepcionPersonal . "'";
	}

	function selectAll() {
		return "select idExcepcionPersonal, descripcion, fecha, profesor_idProfesor, tipo_idTipo
				from excepcionpersonal";
	}

	function selectAllByProfesor() {
		return "select idExcepcionPersonal, descripcion, fecha, profesor_idProfesor, tipo_idTipo
				from excepcionpersonal
				where profesor_idProfesor = '" . $this -> profesor . "'";
	}

	function selectAllByTipo() {
		return "select idExcepcionPersonal, descripcion, fecha, profesor_idProfesor, tipo_idTipo
				from excepcionpersonal
				where tipo_idTipo = '" . $this -> tipo . "'";
	}

	function selectAllOrder($orden, $dir){
		return "select idExcepcionPersonal, descripcion, fecha, profesor_idProfesor, tipo_idTipo
				from excepcionpersonal
				order by " . $orden . " " . $dir;
	}

	function selectAllByProfesorOrder($orden, $dir) {
		return "select idExcepcionPersonal, descripcion, fecha, profesor_idProfesor, tipo_idTipo
				from excepcionpersonal
				where profesor_idProfesor = '" . $this -> profesor . "'
				order by " . $orden . " " . $dir;
	}

	function selectAllByTipoOrder($orden, $dir) {
		return "select idExcepcionPersonal, descripcion, fecha, profesor_idProfesor, tipo_idTipo
				from excepcionpersonal
				where tipo_idTipo = '" . $this -> tipo . "'
				order by " . $orden . " " . $dir;
	}

	function search() {
		return "select idExcepcionPersonal, descripcion, fecha, profesor_idProfesor, tipo_idTipo
				from excepcionpersonal
				where fecha='". $this -> fecha ."' or profesor_idProfesor='". $this -> profesor ."' or tipo_idTipo='". $this -> tipo ."' ";
	}
}
?>
