<?php
require ("fpdf/fpdf.php");
require("business/Administrator.php");
require("business/LogAdministrator.php");
require("business/Profesor.php");
require("business/Tipo.php");
require("business/Inscripcion.php");
require("business/Horario.php");
require("business/Asignatura.php");
require("business/Grupo.php");
require("business/Asistencia.php");
require("business/ExcepcionPersonal.php");
require("business/Excepcion.php");
require("business/LogCoordinador.php");
require("business/Coordinador.php");
require("business/Inasistencia.php");
$profesor="";
if(isset($_GET['profesor'])){
    $profesor = $_GET['profesor'];
}


$objProfesor = new Profesor($profesor);
$objProfesor->select();

$asistencia = new Asistencia("", "", $profesor);
$asistencias = $asistencia->search();

$pdf = new FPDF();

$pdf->AddPage();
$pdf->Image('img/logo1.jpeg',23,8,35,38);

$pdf->SetFont('Times','BI',12);
$pdf->SetXY(95,10); 
$pdf->Cell(40,10,'Universidad Distrital Francisco Jose de Caldas',0,0,'C');
$pdf->SetXY(95,20);
$pdf->Cell(40,10,'Reporte de asistencia por docente',0,0,'C');
$pdf->SetXY(95,30);
$pdf->Cell(40,10,utf8_decode('Docente: '.$objProfesor->getNombre()),0,0,'C');
$pdf->SetXY(45,55); 
$pdf->SetFillColor(206, 214, 224);
$pdf->Cell(60,5,'Fecha',1,0,'C',True);
$pdf->Cell(60,5,'Dia',1,0,'C',True);
$pdf->Ln();
$counter = 60;

$pdf->SetFont('Arial','',12);
foreach($asistencias as $currentAsistencia){
    $dias = array('Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado');
    //echo "  ".date('N', strtotime($currentAsistencia->getFecha()))." ".$currentAsistencia->getFecha();
    $fechas = $dias[date('N', strtotime($currentAsistencia->getFecha()))];
    $pdf->SetXY(45,$counter); 
    $pdf->Cell(60,5,$currentAsistencia->getFecha(),1,0,'C');
    $pdf->Cell(60,5,$fechas,1,0,'C');
    $pdf->Ln();
    $counter=$counter+5;
   }
$pdf->Output();