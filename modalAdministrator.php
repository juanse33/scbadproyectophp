<?php
require("business/Administrator.php");
require("business/LogAdministrator.php");
require("business/Profesor.php");
require("business/Tipo.php");
require("business/Inscripcion.php");
require("business/Horario.php");
require("business/Asignatura.php");
require("business/Grupo.php");
require("business/Asistencia.php");
require("business/ExcepcionPersonal.php");
require("business/Excepcion.php");
require("business/LogCoordinador.php");
require("business/Coordinador.php");
require("business/Inasistencia.php");
require_once("persistence/Connection.php");
$idAdministrator = $_GET ['idAdministrator'];
$administrator = new Administrator($idAdministrator);
$administrator -> select();
?>

<style>
@import url('https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300&display=swap');
@import url('https://fonts.googleapis.com/css?family=Spartan&display=swap" rel="stylesheet');

#card{
	font-family: 'Spartan', sans-serif;
		color:rgb(47, 53, 66);
		font-size:90%;
}
h4, h6{
	color:darkgrey;
	font-size:100%;
}
p{
	font-size:120%;
	border-bottom:solid 1px grey;
	border-color:darkgrey;
}
p:hover{
	border-color:rgb(47, 53, 66);
}

</style>
<br/>
<div class="container " id="card">
	<div class="row">
		<div class="col">
		<br>
		<h1><center> <?php echo $administrator->getName() ?></center></h1>
		</div>
		</div>
		<br>
	<div class="row">	
		<div class="col-3">
		<img src=<?php if($administrator->getPicture() == "") { echo "img/photo.svg"; } else { echo $administrator->getPicture(); } ?> width="100%" style="border-radius: 300px">
		</div>
		<div class="col-9">
			<div class="container">
				<div class="row">
					<div class="col">
						<h4>Nombre</h4>
					</div>
					<div class="col">
						<h4>Apellido</h4>
					</div>
				</div>
				<div class="row">
					<div class="col">
						<p><?php echo $administrator->getName() ?></p>
					</div>
					<div class="col">
					<p><?php echo $administrator->getLastName() ?></p>
					</div>
				</div>
				
				<div class="row">
					<div class="col">
						<h4>Correo Electronico</h4>
					</div>
				</div>
				<div class="row">
					<div class="col">
					<p><?php echo $administrator->getEmail() ?></p>
					</div>
				</div>	
			</div>
		</div>
	</div>
</br>
</br>
</div>
